﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardTilesController : MonoBehaviour {

    public static BoardTilesController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<BoardTilesController>();

            return _instance;
        }
    }
    private static BoardTilesController _instance;

    public BoardTile[][] Board;
    public BoardTile[] BoardTiles;
    public Transform[] BoardEdges;

    private List<BoardTile> _continuousPath = new List<BoardTile>();
    private int _sameLetterIndex = 0;
    private int _upLetterIndex = -1;
    private int _downLetterIndex = 1;

    public float LeftBoardScreenPosition { get { return Camera.main.WorldToScreenPoint(BoardEdges[0].position).x; } }
    public float RightBoardScreenPosition { get { return Camera.main.WorldToScreenPoint(BoardEdges[1].position).x; } }
    public float UpBoardScreenPosition { get { return Camera.main.WorldToScreenPoint(BoardEdges[2].position).y; } }
    public float DownBoardScreenPosition { get { return Camera.main.WorldToScreenPoint(BoardEdges[3].position).y; } }
    public Vector3 CenterBoardScreenPosition { get { return Camera.main.WorldToScreenPoint(BoardEdges[4].position); } }

    void Start ()
    {
        int boardTileIndex = 0;

        Board = new BoardTile[4][];
        
        for (int i = 0; i < 4; i++)
        {
            Board[i] = new BoardTile[4];

            for (int j = 0; j < 4; j++)
            {
                Board[i][j] = BoardTiles[boardTileIndex];
                boardTileIndex++;
            }
        }
	}
	
	void Update ()
    {

    }

    public void ProcessBoardPath(Dinossaur dino, Bushes bush)
    {
        List<BoardTile> pathTiles = new List<BoardTile>();
        int pathTilesIndex = 0;

        float pathShowingDuration = dino.Variables.TimeShowingDirection;

        for (int i = 0; i < dino.Variables.NumberOfPathNodes; i++)
        {
            if(i == 0)
            {
                pathTiles.Add(bush.StartTile);
                pathTiles[pathTilesIndex].SetMarkerToDino(0, pathShowingDuration, dino.MarkerColor);
                AddContinuousPath(pathTiles[pathTilesIndex]);
            }
            else
            {
                BoardTile pathTile = GetRandomTileAround(pathTiles[pathTilesIndex]);

                if(pathTile == null)
                {
                    pathTiles[pathTilesIndex].DirectionArrow.gameObject.SetActive(false);
                    pathTiles[pathTilesIndex].MarkForTrap();
                    pathTiles[pathTilesIndex].SetIconToDino((pathTilesIndex + 1) * 0.25f, pathShowingDuration, dino.HeadIcon);
                    break;
                }

                pathTiles.Add(pathTile);
                pathTilesIndex++;
                pathTiles[pathTilesIndex].SetMarkerToDino(pathTilesIndex * 0.35f, pathShowingDuration, dino.MarkerColor);

                if (pathTilesIndex != dino.Variables.NumberOfPathNodes)
                    pathTiles[pathTilesIndex-1].SetDirectionArrow((pathTilesIndex-1) * 0.35f, GetDirection(pathTiles[pathTilesIndex-1], pathTiles[pathTilesIndex]));

                AddContinuousPath(pathTiles[pathTilesIndex]);

                if (pathTilesIndex + 1 == dino.Variables.NumberOfPathNodes)
                {
                    pathTiles[pathTilesIndex].MarkForTrap();
                    pathTiles[pathTilesIndex].SetIconToDino((pathTilesIndex + 1) * 0.25f, pathShowingDuration, dino.HeadIcon);

                    dino.PathFinalTile = pathTiles[pathTilesIndex];
                }
            }
        }

        ClearContinuousPath(pathTiles);

        bush.ShowHeadIconBySeconds(dino.Variables.TimeShowingDirection, dino.HeadIcon);

        dino.SetBoardPath(pathTiles, bush);
        dino.StartPathWalkDelay();
    }

    private BoardTile GetRandomTileAround(BoardTile startTile)
    {
        List<BoardTile> availableTiles = new List<BoardTile>();
        BoardTile selectedTile = new BoardTile();

        availableTiles.AddRange(GetAvailableTiles(_sameLetterIndex, startTile.LetterIndex, startTile.NumberIndex));
        availableTiles.AddRange(GetAvailableTiles(_upLetterIndex, startTile.LetterIndex, startTile.NumberIndex));
        availableTiles.AddRange(GetAvailableTiles(_downLetterIndex, startTile.LetterIndex, startTile.NumberIndex));

        if(availableTiles.Count > 0)
            selectedTile = availableTiles[Random.Range(0, availableTiles.Count)];

        return selectedTile;
    }

    private List<BoardTile> GetAvailableTiles(int matrixDirection, int startLetterIndex, int startNumberIndex)
    {
        List<BoardTile> availableTiles = new List<BoardTile>();

        int letterIndex = startLetterIndex;
        int numberIndex = startNumberIndex;

        if (letterIndex + matrixDirection >= 0 && letterIndex + matrixDirection <= 3)
        {
            letterIndex = startLetterIndex + matrixDirection;

            if (matrixDirection == 0)
            {
                if (startNumberIndex - 1 >= 0)
                {
                    numberIndex = startNumberIndex - 1;

                    if (TileIsAvailable(Board[letterIndex][numberIndex]))
                        availableTiles.Add(Board[letterIndex][numberIndex]);
                }

                if (startNumberIndex + 1 <= 3)
                {
                    numberIndex = startNumberIndex + 1;

                    if (TileIsAvailable(Board[letterIndex][numberIndex]))
                        availableTiles.Add(Board[letterIndex][numberIndex]);
                }
            }
            else
            {
                numberIndex = startNumberIndex;

                if (TileIsAvailable(Board[letterIndex][numberIndex]))
                    availableTiles.Add(Board[letterIndex][numberIndex]);
            }
        }

        return availableTiles;
    }

    private List<BoardTile> GetAvailableTilesDiagonal(int matrixDirection, int startLetterIndex, int startNumberIndex)
    {
        List<BoardTile> availableTiles = new List<BoardTile>();

        int letterIndex = startLetterIndex;
        int numberIndex = startNumberIndex;

        if (letterIndex + matrixDirection >= 0 && letterIndex + matrixDirection <= 3)
        {
            letterIndex = startLetterIndex + matrixDirection;

            if (matrixDirection != 0)
            {
                numberIndex = startNumberIndex;

                if (TileIsAvailable(Board[letterIndex][numberIndex]))
                    availableTiles.Add(Board[letterIndex][numberIndex]);
            }

            if (startNumberIndex - 1 >= 0)
            {
                numberIndex = startNumberIndex - 1;

                if (TileIsAvailable(Board[letterIndex][numberIndex]))
                    availableTiles.Add(Board[letterIndex][numberIndex]);
            }

            if (startNumberIndex + 1 <= 3)
            {
                numberIndex = startNumberIndex + 1;

                if (TileIsAvailable(Board[letterIndex][numberIndex]))
                    availableTiles.Add(Board[letterIndex][numberIndex]);
            }
        }

        return availableTiles;
    }

    private GameEnums.BoardDirections GetDirection(BoardTile startTile, BoardTile finalTile)
    {
        if(finalTile.LetterIndex == startTile.LetterIndex)
        {
            if (finalTile.NumberIndex > startTile.NumberIndex)
                return GameEnums.BoardDirections.Right;

            if (finalTile.NumberIndex < startTile.NumberIndex)
                return GameEnums.BoardDirections.Left;
        }

        if (finalTile.LetterIndex > startTile.LetterIndex)
        {
            if (finalTile.NumberIndex == startTile.NumberIndex)
                return GameEnums.BoardDirections.Down;

            if (finalTile.NumberIndex > startTile.NumberIndex)
                return GameEnums.BoardDirections.DownRight;

            if (finalTile.NumberIndex < startTile.NumberIndex)
                return GameEnums.BoardDirections.DownLeft;
        }

        if (finalTile.LetterIndex < startTile.LetterIndex)
        {
            if (finalTile.NumberIndex == startTile.NumberIndex)
                return GameEnums.BoardDirections.Up;

            if (finalTile.NumberIndex > startTile.NumberIndex)
                return GameEnums.BoardDirections.UpRight;

            if (finalTile.NumberIndex < startTile.NumberIndex)
                return GameEnums.BoardDirections.UpLeft;
        }

        return GameEnums.BoardDirections.None;
    }

    private GameEnums.BoardDirections GetInverseDirection(GameEnums.BoardDirections direction)
    {
        if (direction == GameEnums.BoardDirections.Left)
            return GameEnums.BoardDirections.Right;
        if (direction == GameEnums.BoardDirections.Right)
            return GameEnums.BoardDirections.Left;
        if (direction == GameEnums.BoardDirections.Up)
            return GameEnums.BoardDirections.Down;
        if (direction == GameEnums.BoardDirections.Down)
            return GameEnums.BoardDirections.Up;

        return GameEnums.BoardDirections.None;
    }

    private bool TileIsAvailable(BoardTile currentTile)
    {
        if (_continuousPath.Contains(currentTile) || currentTile.MarkedForTrap)
            return false;

        return true;
    }

    private void AddContinuousPath(BoardTile boardTile)
    {
        if(!_continuousPath.Contains(boardTile))
        {
            _continuousPath.Add(boardTile);
        }
    }

    private void ClearContinuousPath(List<BoardTile> savedTiles)
    {
        for (int i = 0; i < savedTiles.Count; i++)
        {
            if (_continuousPath.Contains(savedTiles[i]))
            {
                _continuousPath.Remove(savedTiles[i]);
            }
        }
    }
}
