﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComponentControl : MonoBehaviour {

    public Behaviour Component;
    
    public Component SavedComponent { get; set; }

	void Start () {
		
	}
	
	void Update () {

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            DisableComponent();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            EnableComponent();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            DeleteComponent();
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            RestoreComponent();
        }
    }

    public void EnableComponent()
    {
        if (Component != null)
            Component.enabled = true;
    }

    public void DisableComponent()
    {
        if (Component != null)
            Component.enabled = false;
    }

    public void RestoreComponent()
    {
        print(SavedComponent.GetType().GetProperty("Path"));

        if (SavedComponent == null)
            return;

        Component component = gameObject.AddComponent(SavedComponent.GetType());
        component = SavedComponent;

        SavedComponent = null;
    }

    public void DeleteComponent()
    {
        SavedComponent = Instantiate(Component);

        print(SavedComponent.GetType());

        Destroy(Component);
    }
}
