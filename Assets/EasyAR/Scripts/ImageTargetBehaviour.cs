//=============================================================================================================================
//
// Copyright (c) 2015-2017 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
// EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
// and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
//
//=============================================================================================================================

using UnityEngine.Events;

namespace EasyAR
{
    [System.Serializable] public class ImageTargetEvent : UnityEvent { };

    public class ImageTargetBehaviour : ImageTargetBaseBehaviour
    {        
        public ImageTargetEvent OnTargetFound = new ImageTargetEvent();
        public ImageTargetEvent OnTargetLost = new ImageTargetEvent();

        protected override void Awake()
        {
            base.Awake();

            TargetFound += (a) => OnImageTargetFound();
            TargetLost += (a) => OnImageTargetLost();
        }

        public void OnImageTargetFound()
        {
            OnTargetFound.Invoke();
        }
        
        public void OnImageTargetLost()
        {
            OnTargetLost.Invoke();
        }
    }
}
