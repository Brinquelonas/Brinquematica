﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Customizable Character Face" {
	Properties {
		_Color ("Main Color", Color) = (.5,.5,.5,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_FaceTex ("Skin Base (RGB)", 2D) = "white" { }
		_SkinTone ("Skin Tone", Color) = (0.96,0.72,0.46,1)
		_ToonShade ("ToonShader Cubemap(RGB)", CUBE) = "" { }
	}


	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent"}
		Pass {
			Name "BASE"
			Cull Off
			Blend SrcAlpha OneMinusSrcAlpha
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D _FaceTex;
			samplerCUBE _ToonShade;
			float4 _MainTex_ST;
			float4 _FaceTex_ST;
			float4 _Color;
			float4 _SkinTone;

			struct appdata {
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				float3 normal : NORMAL;
			};
			
			struct v2f {
				float4 pos : SV_POSITION;
				float2 texcoord : TEXCOORD0;
				float3 cubenormal : TEXCOORD1;
				UNITY_FOG_COORDS(2)
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.cubenormal = mul (UNITY_MATRIX_MV, float4(v.normal,0));
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 color = tex2D(_MainTex, i.texcoord) * _SkinTone;

				fixed4 face = tex2D(_FaceTex, i.texcoord);

				fixed4 col = lerp(color, face, face.a);
				col *= _Color;

				fixed4 cube = texCUBE(_ToonShade, i.cubenormal) * _Color;

				fixed lum =  dot(col, fixed4(0.2126, 0.7152, 0.0722, 0));

				fixed4 c;

				if (lum > 0.5f)
					c = 1 - (1 - 2 * (col - 0.5f)) * (1 - cube);
				else
					c = (2 * col) * cube;

				UNITY_APPLY_FOG(i.fogCoord, c);
				return c;
			}
			ENDCG			
		}
	} 

	Fallback "VertexLit"
}
