﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum CharacterSex
{
    None,
    Male,
    Female
}

[System.Serializable]
public class CharacterCustomizationPreset
{
    public string Name;
    public CharacterSex Sex;
    public int FaceIndex;
    public int HairIndex;
    public int BodyIndex;
}

[System.Serializable]
public class CharacterCustomizationColorPreset
{
    public string Name;
    public Color HairColor = new Color(0.96f, 0.72f, 0.46f, 1);
    public Color SkinTone = new Color(0.33f, 0.28f, 0.21f, 1);
}

[ExecuteInEditMode]
public class CharacterCustomization : MonoBehaviour {

    [System.Serializable]
    public class CharacterCustomizationEvent : UnityEvent { }

    [System.Serializable]
    public struct CharacterParts
    {
        public GameObject Head;
        public GameObject Body;

        public CharacterParts(GameObject head, GameObject body)
        {
            Head = head;
            Body = body;
        }
    }

    [Header("Presets")]
    public List<CharacterCustomizationPreset> Presets;
    public List<CharacterCustomizationColorPreset> ColorPresets;

    [SerializeField]
    private int _presetIndex;
    public int PresetIndex
    {
        get
        {
            return _presetIndex;
        }
        set
        {
            _presetIndex = value;
            _presetIndex = Mathf.Clamp(_presetIndex, 0, Presets.Count - 1);

            FaceIndex = Presets[_presetIndex].FaceIndex;
            HairStyleIndex = Presets[_presetIndex].HairIndex;
            BodyIndex = Presets[_presetIndex].BodyIndex;
        }
    }

    [SerializeField]
    private int _colorPresetIndex;
    public int ColorPresetIndex
    {
        get
        {
            return _colorPresetIndex;
        }
        set
        {
            _colorPresetIndex = value;
            _colorPresetIndex = Mathf.Clamp(_colorPresetIndex, 0, ColorPresets.Count - 1);

            SkinColor = ColorPresets[_colorPresetIndex].SkinTone;
            HairColor = ColorPresets[_colorPresetIndex].HairColor;
        }
    }

    [Header("Body")]
    public CharacterParts BodyParts;
    public List<GameObject> HairStyles = new List<GameObject>();

    [SerializeField]
    private int _hairStyleIndex;
    public int HairStyleIndex
    {
        get
        {
            return _hairStyleIndex;
        }
        set
        {
            _hairStyleIndex = value;
            _hairStyleIndex = Mathf.Clamp(_hairStyleIndex, 0, HairStyles.Count - 1);

            for (int i = 0; i < HairStyles.Count; i++)
            {
                HairStyles[i].SetActive(i == _hairStyleIndex);
            }
        }
    }

    public List<Texture2D> Faces = new List<Texture2D>();

    [SerializeField]
    private int _faceIndex;
    public int FaceIndex
    {
        get
        {
            return _faceIndex;
        }
        set
        {
            _faceIndex = value;
            _faceIndex = Mathf.Clamp(_faceIndex, 0, Faces.Count - 1);

            BodyParts.Head.GetComponent<Renderer>().sharedMaterial.SetTexture("_FaceTex", Faces[_faceIndex]);
        }
    }

    public List<GameObject> BodyTypes = new List<GameObject>();

    [SerializeField]
    private int _bodyIndex;
    public int BodyIndex
    {
        get
        {
            return _bodyIndex;
        }
        set
        {
            _bodyIndex = value;
            _bodyIndex = Mathf.Clamp(_bodyIndex, 0, BodyTypes.Count - 1);

            for (int i = 0; i < BodyTypes.Count; i++)
            {
                BodyTypes[i].SetActive(i == _bodyIndex);
            }
            BodyParts.Body = BodyTypes[_bodyIndex];
        }
    }

    [Header("Colors")]
    [SerializeField]
    private Color _skinColor = new Color(0.33f, 0.28f, 0.21f, 1);
    public Color SkinColor
    {
        get
        {
            return _skinColor;
        }
        set
        {
            _skinColor = value;
            SetColor("_SkinTone", SkinColor);
        }
    }

    [SerializeField]
    private Color _hairColor = new Color(0.96f, 0.72f, 0.46f, 1);
    public Color HairColor
    {
        get
        {
            return _hairColor;
        }
        set
        {
            _hairColor = value;
            for (int i = 0; i < HairStyles.Count; i++)
            {
                HairStyles[i].GetComponent<Renderer>().sharedMaterial.SetColor("_Color", _hairColor);
            }
        }
    }

    [SerializeField]
    private Color _shirtColor = new Color(1, 0, 0, 1);
    public Color ShirtColor
    {
        get
        {
            return _shirtColor;
        }
        set
        {
            _shirtColor = value;
            SetColor("_ShirtColor", _shirtColor);
        }
    }

    [SerializeField]
    private Color _pantsColor = new Color(0, 0.5f, 1, 1);
    public Color PantsColor
    {
        get
        {
            return _pantsColor;
        }
        set
        {
            _pantsColor = value;
            SetColor("_PantsColor", _pantsColor);
        }
    }

    [SerializeField]
    private Color _shoesColor = new Color(.2f, .2f, .2f, 1);
    public Color ShoesColor
    {
        get
        {
            return _shoesColor;
        }
        set
        {
            _shoesColor = value;
            SetColor("_ShoesColor", _shoesColor);
        }
    }

    [Header("Events")]
    public CharacterCustomizationEvent OnConfirm = new CharacterCustomizationEvent();

    private Renderer[] _renderers
    {
        get
        {
            return GetComponentsInChildren<Renderer>(true);
        }
    }

    private void Start()
    {
        if (BodyParts.Head == null)
            BodyParts.Head = transform.Find("Head").gameObject;

        if (BodyParts.Body == null)
            BodyParts.Body = transform.Find("Body").gameObject;
    }

    private void Update()
    {
        if (Application.isEditor && !Application.isPlaying)
        {
            PresetIndex = _presetIndex;
            ColorPresetIndex = _colorPresetIndex;
            HairStyleIndex = _hairStyleIndex;
            FaceIndex = _faceIndex;
            BodyIndex = _bodyIndex;
            SkinColor = _skinColor;
            HairColor = _hairColor;
            ShirtColor = _shirtColor;
            PantsColor = _pantsColor;
            ShoesColor = _shoesColor;
        }
    }

    public void SetColor(string propertyName, Color color)
    {
        for (int i = 0; i < _renderers.Length; i++)
        {
            if (!_renderers[i].sharedMaterial.HasProperty(propertyName))
                continue;

            Color currentColor = _renderers[i].sharedMaterial.GetColor(propertyName);
            Color newColor = color;
            newColor.a = currentColor.a;
            _renderers[i].sharedMaterial.SetColor(propertyName, newColor);
        }
    }

    public void SwitchPresetAnimation(int bodyIndex, int colorIndex, System.Action callback = null)
    {
        PotaTween tween = PotaTween.Get(gameObject, "Switch");
        tween.Stop();
        tween.Reverse(() => 
        {
            PresetIndex = bodyIndex;
            ColorPresetIndex = colorIndex;
            tween.Play(null);
        });
    }

#region Methods for Unity Events
    public void AddBodyPresetIndex()
    {
        int index = PresetIndex;
        index++;
        index %= Presets.Count;
        PresetIndex = index;        
    }

    public void SetBodyPresetIndex(int index)
    {
        //PresetIndex = index;
        SwitchPresetAnimation(index, _colorPresetIndex);
    }

    public void SetColorPresetIndex(int index)
    {
        //ColorPresetIndex = index;
        SwitchPresetAnimation(_presetIndex, index);
    }

    public void ConfirmCustomization()
    {
        OnConfirm.Invoke();
    }
    #endregion
}
