﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class DraggableElement : UIBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [System.Serializable]
    public class DragEvent : UnityEvent<DraggableElement> { }

    public DragEvent OnBegin = new DragEvent();
    public DragEvent OnDragging = new DragEvent();
    public DragEvent OnEnd = new DragEvent();
    public Vector3 StartPosition { get; set; }
    Transform startParent;

    public virtual void OnBeginDrag(PointerEventData eventData)
    {
        StartPosition = transform.position;
        OnBegin.Invoke(this);
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        //transform.position = Input.mousePosition;
        OnDragging.Invoke(this);
    }

    public virtual void OnEndDrag(PointerEventData eventData)
    {
        OnEnd.Invoke(this);
    }
}
