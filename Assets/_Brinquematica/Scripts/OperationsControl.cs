﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperationsControl : MonoBehaviour {

    public Transform Character;
    public Transform[] Operations;

	void Start ()
    {
        Character.gameObject.SetActive(false);
	}
	
	void Update ()
    {
		
	}

    public void SetCharacterToOperation(int operationIndex)
    {
        Character.gameObject.SetActive(true);
        
        Character.SetParent(Operations[operationIndex]);
    }
}
