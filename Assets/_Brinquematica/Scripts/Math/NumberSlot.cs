﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NumberSlot : MonoBehaviour {

    private TextMeshProUGUI _textMesh;
    public TextMeshProUGUI TextMesh
    {
        get
        {
            if (_textMesh == null)
                _textMesh = GetComponentInChildren<TextMeshProUGUI>();

            return _textMesh;
        }
    }

    private DraggableNumber _assignedNumber;
    public DraggableNumber AssignedNumber
    {
        get
        {
            return _assignedNumber;
        }

        set
        {
            _assignedNumber = value;
            string txt = "";
            if (_assignedNumber != null)
                txt = _assignedNumber.Value.ToString();
            TextMesh.text = txt;
        }
    }

}
