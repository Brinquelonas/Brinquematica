﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Acceleration : MonoBehaviour {

    public Text AccelerationText;
    public Text GyroscopeText;
    public Text CompassText;
    public Text LocationText;

    void Start ()
    {
        Input.location.Start();
    }
	
	void Update ()
    {
        //AccelerationText.text = "Acceleration - X:" + Input.acceleration.x + " / Y:" + Input.acceleration.y + " / Z:" + Input.acceleration.z;
        //GyroscopeText.text = SystemInfo.supportsGyroscope ? "Gyroscope Enabled - X:" +  (Input.gyro.rotationRate.x + " / Y:" + Input.gyro.rotationRate.y + " / Z:" + Input.gyro.rotationRate.z) : "Gyroscope Disabled";

        AccelerationText.text = "Acceleration: " + Input.acceleration;

        if (SystemInfo.supportsGyroscope)
            GyroscopeText.text = "True";
        else
            GyroscopeText.text = "False";

        CompassText.text = "Compass - North:" + Input.compass.magneticHeading;
        LocationText.text = "Location - Altitude:" + Input.location.lastData.altitude + " / Latitude:" + Input.location.lastData.latitude + " / Longitude:" + Input.location.lastData.longitude;
    }
}
