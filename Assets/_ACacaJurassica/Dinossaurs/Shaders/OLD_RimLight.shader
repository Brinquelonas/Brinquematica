﻿Shader "Example/OLD/Rim" {
    Properties {
	  _Color ("Tint", Color) = (1,1,1,1)
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
    }
    SubShader {
      Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector"="True" }
	  Cull Off
	  //Blend SrcAlpha OneMinusSrcAlpha

      CGPROGRAM
      #pragma surface surf Lambert

      struct Input {
          float2 uv_MainTex;
          float2 uv_BumpMap;
          float3 viewDir;
      };

	  float4 _Color;
      sampler2D _MainTex;
      sampler2D _BumpMap;
      float4 _RimColor;
      float _RimPower;

      void surf (Input IN, inout SurfaceOutput o) {
		  half4 c = tex2D (_MainTex, IN.uv_MainTex);
          o.Albedo = c.rgb * _Color.rgb;
          o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_BumpMap));
          half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
          o.Emission = _RimColor.rgb * pow (rim, _RimPower);
		  o.Alpha = c.a * _Color.a;
      }

      ENDCG

    } 
    Fallback "Diffuse"
  }