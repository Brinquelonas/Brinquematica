﻿// Upgrade NOTE: commented out 'float4x4 _Object2World', a built-in variable
// Upgrade NOTE: replaced '_LightMatrix0' with 'unity_WorldToLight'
// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Example/Rim" {
    Properties {
	  _Color ("Tint", Color) = (1,1,1,1)
      _MainTex ("Texture", 2D) = "white" {}
      _BumpMap ("Bumpmap", 2D) = "bump" {}
      _RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
      _RimPower ("Rim Power", Range(0.5,8.0)) = 3.0
    }
    SubShader {
      Tags { "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector"="True" }
	  //Cull Off
	  //Blend SrcAlpha OneMinusSrcAlpha

	  Pass {
             // Only render alpha channel
             ColorMask A
             Blend SrcAlpha OneMinusSrcAlpha
 
             CGPROGRAM
             #pragma vertex vert
             #pragma fragment frag
 
             fixed4 _Color;
 
             float4 vert(float4 vertex : POSITION) : SV_POSITION {
                 return UnityObjectToClipPos(vertex);
             }
 
             fixed4 frag() : SV_Target {
                 return _Color;
             }
 
             ENDCG
         }
		 
	  Pass 
	  {
	  //Tags { "LightMode" = "ForwardBase" }
			ColorMask RGB
            Blend SrcAlpha OneMinusSrcAlpha

	  CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            // compile shader into multiple variants, with and without shadows
            // (we don't care about any lightmaps yet, so skip these variants)
            //#pragma multi_compile_fwdbase nolightmap nodirlightmap nodynlightmap novertexlight
            // shadow helper functions and macros
            #include "AutoLight.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                SHADOW_COORDS(1) // put shadows data into TEXCOORD1
                fixed3 diff : COLOR0;
                fixed3 ambient : COLOR1;
                float4 pos : SV_POSITION;

				half3 tspace0 : TEXCOORD1;
                half3 tspace1 : TEXCOORD2;
                half3 tspace2 : TEXCOORD3;

				float3 worldPos : TEXCOORD4;
            };
			
            float4 _Color;
            sampler2D _MainTex;
			float4 _MainTex_ST;
            sampler2D _BumpMap;
            float4 _BumpMap_ST;
            float4 _RimColor;
            float _RimPower;

            v2f vert (float4 vertex : POSITION, float3 normal : NORMAL, float4 tangent : TANGENT, float2 uv : TEXCOORD0)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(vertex);
                o.uv = uv;
				o.worldPos = mul(unity_ObjectToWorld, vertex).xyz;
                half3 worldNormal = UnityObjectToWorldNormal(normal);
                half nl = max(0, dot(worldNormal, _WorldSpaceLightPos0.xyz));

				half3 wNormal = UnityObjectToWorldNormal(normal);
                half3 wTangent = UnityObjectToWorldDir(tangent.xyz);
                half tangentSign = tangent.w * unity_WorldTransformParams.w;
                half3 wBitangent = cross(wNormal, wTangent) * tangentSign;
                o.tspace0 = half3(wTangent.x, wBitangent.x, wNormal.x);
                o.tspace1 = half3(wTangent.y, wBitangent.y, wNormal.y);
                o.tspace2 = half3(wTangent.z, wBitangent.z, wNormal.z);

                o.diff = nl * _LightColor0.rgb;
                o.ambient = ShadeSH9(half4(worldNormal,1));
                // compute shadows data
                TRANSFER_SHADOW(o)
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, _MainTex_ST.xy * i.uv.xy + _MainTex_ST.zw) * _Color;

				half3 tnormal = UnpackNormal(tex2D(_BumpMap, _BumpMap_ST.xy * i.uv.xy + _BumpMap_ST.zw));
                half3 worldNormal;
                worldNormal.x = dot(i.tspace0, tnormal);
                worldNormal.y = dot(i.tspace1, tnormal);
                worldNormal.z = dot(i.tspace2, tnormal);

				/*float attenuation; 
				if (0.0 == _WorldSpaceLightPos0.w) // directional light?
				{
				   attenuation = 1.0; // no attenuation
				}
				else // point or spot light
				{
				   float3 vertexToLightSource =
					  float3(_WorldSpaceLightPos0 - i.worldPos);
				   float distance = length(vertexToLightSource);
				   attenuation = 1.0 / distance;
				} 
				float3 diffuse = attenuation * float3(_LightColor0.rgb);
				col *= float4(diffuse,1);*/
				
                // compute shadow attenuation (1.0 = fully lit, 0.0 = fully shadowed)
                fixed shadow = SHADOW_ATTENUATION(i);
                // darken light's illumination with shadow, keep ambient intact
                fixed3 lighting = i.diff * shadow * 0.5 + i.ambient + shadow * 0.5;
                col.rgb *= lighting;


                half3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));
				half rim = 1.0 - saturate(dot (normalize(worldViewDir), worldNormal));
				col.rgb += _RimColor.rgb * pow (rim, _RimPower);

                return col;
            }
            ENDCG
        }

		/*Pass {
         Tags { "LightMode" = "ForwardAdd" }
         Blend One One // additive blending
 
          CGPROGRAM
         #pragma vertex vert  
         #pragma fragment frag
 
         uniform sampler2D _MainTex;
         ///////
         // uniform float4x4 _Object2World;
         //uniform float4 _WorldSpaceLightPos0;
         //uniform float4 _LightPositionRange;
         uniform float4 _LightColor0;
         uniform float4x4 unity_WorldToLight;
         uniform sampler2D _LightTexture0;
       
         struct vertexInput {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float4 texcoord : TEXCOORD0;
         };
         struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 uv : TEXCOORD0;
            float4 posWorld : TEXCOORD1;
         };
 
         vertexOutput vert(vertexInput input)
         {
            vertexOutput output;
            float4x4 modelMatrix = unity_ObjectToWorld;
 
            float4 tilePosition = float4(input.normal, input.vertex.a);
            output.posWorld = mul(modelMatrix, tilePosition);
            output.pos = UnityObjectToClipPos(input.vertex);
            output.uv = input.texcoord;
            return output;
         }
 
         float4 frag(vertexOutput input) : COLOR
         {
            float attenuation;
 
            if (0.0 == _WorldSpaceLightPos0.w) // directional light?
            {
               attenuation = 1.0; // no attenuation
            }
            else // point or spot light
            {
               //// It does linear attenuation if I have this code instead:
               //float3 vertexToLightSource =
               //   float3(_WorldSpaceLightPos0 - input.posWorld);
               //float distance = length(vertexToLightSource);
               //attenuation = 1.0 / distance;
 
               attenuation = tex2D(_LightTexture0, dot(_WorldSpaceLightPos0,_WorldSpaceLightPos0).rr).a;
            }
 
            float3 diffuse = attenuation * float3(_LightColor0.rgb);
 
            float4 c = tex2D(_MainTex, input.uv);
            c.rgb *= diffuse;
            return c;
         }
 
         ENDCG
      }*/

        // shadow casting support
        UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
    } 
    Fallback "Diffuse"
  }