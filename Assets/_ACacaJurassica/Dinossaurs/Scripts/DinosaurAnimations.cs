﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinosaurAnimations : MonoBehaviour {

    public enum Anims
    {
        None,
        Idle,
        Running,
        Eating,
        Appear,
        Captured,
        Hit
    }
    
    public Anims CurrentAnimation;

    private System.Action Callback;

    [SerializeField]
    private bool _inBush;
    public bool InBush
    {
        get
        {
            return _inBush;
        }
        set
        {
            _inBush = value;

            Animator.SetBool("InBush", _inBush);
        }
    }

    private Animator _animator;
    public Animator Animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();

            return _animator;
        }
    }

    private void Update()
    {
        if (Animator.GetCurrentAnimatorStateInfo(0).loop == false && Animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1 && !Animator.IsInTransition(0))
            if (Callback != null) Callback();
    }
    
    public void PlayAnimation(Anims animation, System.Action callback = null)
    {
        //print(transform.parent.name + CurrentAnimation + " : " + animation);

        if (CurrentAnimation == animation)
            return;

        if (animation == Anims.Appear)
        {
            Animator.SetTrigger("Appear");
            return;
        }
        else if (animation == Anims.Hit)
        {
            Animator.SetTrigger("Hit");
            return;
        }

        CurrentAnimation = animation;

        Animator.SetBool("Idle", animation.ToString() == "Idle");
        Animator.SetBool("Running", animation.ToString() == "Running");
        Animator.SetBool("Eating", animation.ToString() == "Eating");
        Animator.SetBool("Captured", animation.ToString() == "Captured");
    }
}
