﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DinossaurController : MonoBehaviour {

    public static DinossaurController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<DinossaurController>();

            return _instance;
        }
    }
    private static DinossaurController _instance;

    public Dinossaur[] DinossaurPrefabs;
    public Transform DinossaursContainer;

    private List<Dinossaur> _dinossaurs = new List<Dinossaur>();
    public Dictionary<int, int> DinosaursForCollection { get; set; }

    void Start()
    {
        DinosaursForCollection = new Dictionary<int, int>();
    }

    void Update()
    {

    }

    public Dinossaur SpawnDinossaur(Bushes bush)
    {
        int dinoIndex = _dinossaurs.Count;

        _dinossaurs.Add(GameController.Instance.CurrentLevelVariables.DinosaurToSpawn());
        _dinossaurs[dinoIndex].transform.position = bush.Position;
        _dinossaurs[dinoIndex].transform.parent = DinossaursContainer;

        _dinossaurs[dinoIndex].transform.localEulerAngles = Vector3.zero;
        _dinossaurs[dinoIndex].transform.localScale = Vector3.one;
        _dinossaurs[dinoIndex].transform.localPosition = new Vector3(_dinossaurs[dinoIndex].transform.localPosition.x, -0.01f, _dinossaurs[dinoIndex].transform.localPosition.z);

        return _dinossaurs[dinoIndex];
    }

    public void StopDinossaurs()
    {
        for (int i = 0; i < _dinossaurs.Count; i++)
        {
            if(_dinossaurs[i] != null)
                _dinossaurs[i].StopDinossaur();
        }
    }

    public void MarkDinosaurForCollection(int dinoIndex)
    {
        if(DinosaursForCollection.ContainsKey(dinoIndex))
        {
            int value = DinosaursForCollection[dinoIndex];
            DinosaursForCollection[dinoIndex] = value + 1;
        }
        else
        {
            DinosaursForCollection.Add(dinoIndex, 1);
        }
    }

    public void SendDinosaursToCollection()
    {
        for (int i = 0; i < DinosaursForCollection.Count; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                if (DinosaursForCollection.ContainsKey(j))
                {
                    CollectionController.UpdateCollectionItem(j, DinosaursForCollection[j]);
                    break;
                }
            }
        }
    }
}
