﻿using EasyAR;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum GameTableDigitalState { Searching, Updating, Ready }

public class GameBoard : MonoBehaviour {

    public Transform GameBoardDigital;
    public ImageTargetBehaviour GameBoardCode;
    public ImageTrackerBaseBehaviour BoardTracker;
    public ImageTargetBehaviour[] TrapsTracker;

    public Button LoadTrapsButton;

    public GameTableDigitalState TableDigitalState { get; set; }


    void Start ()
    {
        LoadTrapsButton.onClick.AddListener(() => DisableBoardTracker());
        LoadTrapsButton.interactable = false;

        GameBoardCode.OnTargetFound.AddListener(() =>
        {
            if (!GameController.Instance.StartedGame)
                GameController.Instance.StartGame();

            LoadTrapsButton.interactable = true;
        });

        for (int i = 0; i < TrapsTracker.Length; i++)
        {
            TrapsTracker[i].OnTargetFound.AddListener(() =>
            {
                EnableBoardTracker();
            });
        }
    }

    void Update ()
    {
		
    }

    private void DisableBoardTracker()
    {
        SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

        LoadTrapsButton.interactable = false;
        BoardTracker.StopTrack();

        Invoke("EnableBoardTracker", 15);
    }

    private void EnableBoardTracker()
    {
        LoadTrapsButton.interactable = true;
        BoardTracker.StartTrack();
    }
}
