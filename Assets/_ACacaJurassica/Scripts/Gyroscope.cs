﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gyroscope : MonoBehaviour {

    public Transform GyroscopeTarget;
    public Text GyroText;

    public bool GyroscopeSupported { get { return SystemInfo.supportsGyroscope; } }

	void Start ()
    {
        /*void GyroModifyCamera()
        {
            transform.rotation = GyroToUnity(Input.gyro.attitude);
        }

        private static Quaternion GyroToUnity(Quaternion q)
        {
            return new Quaternion(q.x, q.y, -q.z, -q.w);
        }*/

        Input.gyro.enabled = true;

        GyroText.text = SystemInfo.supportsGyroscope? "Gyro Enabled" : "Gyro Disabled";
    }
	
	void Update ()
    {
		if(!SystemInfo.supportsGyroscope)
        {
            enabled = false; 
            return;
        }

        Quaternion gyroscopeRotation = new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y, -Input.gyro.attitude.z, -Input.gyro.attitude.w);
        GyroscopeTarget.rotation = gyroscopeRotation;
        GyroText.text = "Gyro Enabled " + gyroscopeRotation;
    }
}
