﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinosaurModel : MonoBehaviour {

    public Transform Body { get; set; }
    public Transform Head { get; set; }
    public Transform Eyes { get; set; }
    public Transform Fins { get; set; }
    public Transform Membranes { get; set; }
    public Transform Spikes { get; set; }

    public Renderer BodyRender { get; set; }
    public Renderer HeadRender { get; set; }
    public Renderer EyesRender { get; set; }
    public Renderer FinsRender { get; set; }
    public Renderer MembranesRender { get; set; }
    public Renderer SpikesRender { get; set; }

    public DinosaurAnimations Animation { get; set; }

    void Start ()
    {
        Body = transform.Find("Body");
        Head = transform.Find("Head");
        Eyes = transform.Find("Eyes");
        Fins = transform.Find("Fins");
        Membranes = transform.Find("Membranes");
        Spikes = transform.Find("Spikes");

        BodyRender = Body.GetComponent<Renderer>();
        HeadRender = Head.GetComponent<Renderer>();
        EyesRender = Eyes.GetComponent<Renderer>();
        FinsRender = Fins.GetComponent<Renderer>();
        MembranesRender = Membranes.GetComponent<Renderer>();
        SpikesRender = Spikes.GetComponent<Renderer>();

        Animation = GetComponent<DinosaurAnimations>();
    }

    public void Initialize()
    {
        Start();
    }
	
	void Update ()
    {
		
	}
}
