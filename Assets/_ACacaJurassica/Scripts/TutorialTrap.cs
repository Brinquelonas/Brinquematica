﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrap : MonoBehaviour {
    
    public TrapDigitalDrag Drag;
    public TrapAnimations Animations;
    public TrapCollider TrapBase;
    public Food TrapFood;
    public GameEnums.BoardTileId TileToSet;

    public BoardTile SettedTile { get; set; }
    public Dinossaur CatchedDino { get; set; }

    private bool _blockDrag;
    private bool _readyToCapture;

    void Start()
    {
        _readyToCapture = false;

        TrapBase.TriggerEnter += StartedCollision;

        Drag.OnDragBegin += () =>
        {
            if (_blockDrag)
                return;

            Drag.transform.SetParent(transform.parent);
        };

        Drag.OnDragEnd += () =>
        {
            if (_blockDrag)
                return;

            Drag.transform.SetParent(transform);
            Drag.transform.localPosition = new Vector3(0, 0.1f, 0);
        };

        Drag.OnChangeBoardTile += (b) =>
        {
            if (_blockDrag)
                return;

            transform.localPosition = new Vector3(b.transform.localPosition.x, 0, b.transform.localPosition.z);
        };
    }

    void Update()
    {

    }

    private void StartedCollision(Collider other)
    {
        if (other.CompareTag("BoardTile"))
        {
            SettedTile = other.GetComponent<BoardTile>();

            if(SettedTile.Id == TileToSet)
            {
                _blockDrag = true;
            }
        }

        if (other.CompareTag("Dinossaur"))
        {
            Dinossaur dino = other.GetComponent<Dinossaur>();

            if (SettedTile != null && SettedTile.Id == TileToSet)
            {
                CatchedDino = dino;

                if(MainMenu.GameModeSelected == GameEnums.GameType.Experient)
                {
                    TrapFood.FoodDisappearing.SetDuration(CatchedDino.Variables.TimeToEatFood).Play();
                    CatchedDino.StartEating();

                    Invoke("CatchDino", 2.5f);
                }

                if (MainMenu.GameModeSelected == GameEnums.GameType.Professional)
                {
                    CatchedDino.StartEating(false);

                    _readyToCapture = true;
                }
            }
        }
    }

    public void CaptureOnTarget()
    {
        if(MainMenu.GameModeSelected == GameEnums.GameType.Professional && _readyToCapture)
        {
            print("captured");

            CatchDino();
        }
    }

    private void CatchDino()
    {
        TutorialTrapController.Instance.PlayCapturedText();
        TutorialTrapController.Instance.UpdatePoints(CatchedDino.Variables.PointsPerCapture);
        CollectionController.UpdateCollectionItem(0, 3);

        SfxController.Instance.Play(SfxController.Instance.SfxDinoVoice1);

        Destroy(CatchedDino.gameObject);
        Destroy(gameObject);
    }
}
