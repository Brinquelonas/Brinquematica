﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyAR;
using UnityEngine.UI;

public class GameTableTra : MonoBehaviour {

    public ImageTargetBehaviour GameCodeBoard;
    public ImageTargetBehaviour[] GameCodeTrap;
    //public Transform Table;

    public ImageTrackerBaseBehaviour ImageTrackerBoard;
    public ImageTrackerBaseBehaviour ImageTrackerTrap;

    public Button LoadTrapButton;

    public ImageTargetBehaviour CodeBoard { get; set; }
    public bool Loading { get; set; }

    void Start()
    {
        ImageTrackerBoard.TargetUnload += (tracker, target, t, loaded) =>
        {
            print("unloaded: " + loaded);
            //ImageTrackerBoard.StartTrack();
        };

        ImageTrackerBoard.TargetLoad += (tracker, target, t, loaded) =>
        {
            print("loaded: " + loaded);
            //ImageTrackerBoard.StartTrack();
        };

        CodeBoard = Instantiate(GameCodeBoard);
        CodeBoard.Bind(ImageTrackerBoard);
        CodeBoard.SetupWithImage("LonaJpg.jpg", StorageType.Assets, "Lona", new Vector2());

        LoadTrapButton.onClick.AddListener(() =>
        {
            print("unload board");
            ImageTrackerBoard.UnloadImageTargetBehaviour(CodeBoard);
            Destroy(CodeBoard.gameObject);

            Invoke("ReloadGameTable", 10);

            Loading = true;
            LoadTrapButton.interactable = false;
        });

        for (int i = 0; i < GameCodeTrap.Length; i++)
        {
            GameCodeTrap[i].OnTargetFound.AddListener(() =>
            {
                if(Loading)
                {
                    /*CancelInvoke("ReloadGameTable");
                    ReloadGameTable();*/
                }
            });
        }
    }

    void ReloadGameTable()
    {
        if (!Loading)
            return;

        print("reload board");
        //ImageTrackerBoard.StartTrack();
        //ImageTrackerBoard.LoadImageTargetBehaviour(GameCodeBoard);
        CodeBoard = Instantiate(GameCodeBoard);
        CodeBoard.Bind(ImageTrackerBoard);
        CodeBoard.SetupWithImage("LonaJpg.jpg", StorageType.Assets, "Lona", new Vector2());


        Loading = false;
        LoadTrapButton.interactable = true;
    }

    /*private void StartTrapsTrack()
    {
        print("unload board");
        ImageTrackerBoard.UnloadImageTargetBehaviour(GameCodeBoard);
        ImageTrackerBoard.StopTrack();

        print("start trap track");
        ImageTrackerTrap.StartTrack();
    }

    private void StopTrapsTrack()
    {
        print("load board");
        ImageTrackerBoard.LoadImageTargetBehaviour(GameCodeBoard);
        ImageTrackerBoard.StartTrack();

        ImageTrackerTrap.StopTrack();
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            print("unload board");
            ImageTrackerBoard.UnloadImageTargetBehaviour(GameCodeBoard);
            ImageTrackerBoard.StopTrack();
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            print("load board");
            ImageTrackerBoard.LoadImageTargetBehaviour(GameCodeBoard);
            ImageTrackerBoard.StartTrack();
        }


        if (Input.GetKeyDown(KeyCode.A))
        {
            print("unload trap");
            ImageTrackerTrap.UnloadImageTargetBehaviour(GameCodeTrap);
            ImageTrackerTrap.StopTrack();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            print("load trap");
            ImageTrackerTrap.LoadImageTargetBehaviour(GameCodeTrap);
            ImageTrackerTrap.StartTrack();
        }
    }*/
}
