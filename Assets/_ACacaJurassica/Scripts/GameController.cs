﻿using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameController>();

            return _instance;
        }
    }
    private static GameController _instance;

    public GameEnums.GameType Type;
    public DinossaurController DinosController;
    public BoardTilesController BoardController;
    public BushesController BushController;
    public Transform DinossaursContainer;
    public Text Timer;

    public bool StartedGame { get; set; }
    public bool PausedGame { get; set; }
    public bool TimerEnded { get; set; }
    public float Points { get; set; }
    public GameLevelVariables CurrentLevelVariables { get; set; }

    private float _gameTimer = 0;
    private float _timeRate = 1;

    void Start ()
    {
        //CurrentLevelVariables = Type == GameEnums.GameType.Experient ? LevelSelectionExperient.CurrentLevelVariables : LevelSelectionProfessional.CurrentLevelVariables;

        AudioController.Instance.Play();

        if (Type == GameEnums.GameType.Experient)
        {
            CurrentLevelVariables = LevelSelectionExperient.CurrentLevelVariables;
        }
        else if (Type == GameEnums.GameType.Professional)
        {
            CurrentLevelVariables = LevelSelectionProfessional.CurrentLevelVariables;
        }
        else
        {
            CurrentLevelVariables = LevelSelectionBeginner.CurrentLevelVariables;

            Debug.LogWarning("TESTE AQUI");
            StartGame();
        }

        _gameTimer = CurrentLevelVariables.LevelTotalTime;
        Timer.text = Mathf.Floor(_gameTimer / 60).ToString("00") + ":" + Mathf.Floor(_gameTimer % 60).ToString("00");
        TimerEnded = false;
    }

    void Update ()
    {
        if (!StartedGame)
            return;

        if (TimerEnded)
            return;

        _gameTimer -= Time.deltaTime;

        Timer.text = Mathf.Floor(_gameTimer / 60).ToString("00") + ":" + Mathf.Floor(_gameTimer % 60).ToString("00");

        if (_gameTimer <= 0.0f)
        {
            Timer.text = "00:00";
            TimerEnded = true;
            CallTimerEnded();
        }
    }

    public void StartGame()
    {
        if(!StartedGame)
        {
            Invoke("SpawnDinossaur", CurrentLevelVariables.TimeToSpawnFirstDinosaur);

            StartedGame = true;
        }
    }

    public void ResumeGame()
    {
        if(PausedGame)
        {
            /*for (int i = 0; i < _dinossaurs.Count; i++)
            {
                _dinossaurs[i].DinossaurPathWalk.Play();
            }

            Invoke("SpawnNewDino", 2.5f);*/

            PausedGame = false;
        }
    }

    public void PauseGame()
    {
        if(!PausedGame)
        {
            /*for (int i = 0; i < _dinossaurs.Count; i++)
            {
                _dinossaurs[i].DinossaurPathWalk.Pause();
            }

            CancelInvoke("SpawnNewDino");*/

            PausedGame = true;
        }
    }

    private void CallTimerEnded()
    {
        CancelInvoke();
        DinosController.StopDinossaurs();

        GameCanvas.Instance.Timer.Reverse();
        GameCanvas.Instance.Points.Reverse();
        GameCanvas.Instance.TrapsButton.Reverse();

        DinosController.SendDinosaursToCollection();
        LevelCompleted.Instance.SetLevelCompletedValues();
        LevelCompletedCanvas.Instance.Open();
    }

    public void SpawnDinossaur()
    {
        Bushes bush = BushController.GetRandomBush();
        Dinossaur dino = DinosController.SpawnDinossaur(bush);

        if (Type != GameEnums.GameType.Beginner)
            BoardController.ProcessBoardPath(dino, bush);
        else
        {
            dino.transform.localScale = Vector3.one * 2;
            dino.transform.localEulerAngles = new Vector3(0, 180, 0);

            float timeToRunDino = dino.Variables.TimeToEatFood * _timeRate;
            timeToRunDino = Mathf.Clamp(timeToRunDino, CurrentLevelVariables.MinimumTimeToEat, dino.Variables.TimeToEatFood);
            dino.Appear(timeToRunDino);
            BushController.ShakeBush(bush);

            _timeRate -= CurrentLevelVariables.TimeDecreasingRate;

            if (_timeRate < CurrentLevelVariables.MinimumTimeToSpawn)
                _timeRate = CurrentLevelVariables.MinimumTimeToSpawn;
        }

        Invoke("SpawnDinossaur", CurrentLevelVariables.TimeToSpawnDinosaur * _timeRate);
    }
}
