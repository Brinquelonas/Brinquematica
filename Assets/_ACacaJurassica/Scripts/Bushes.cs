﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bushes : MonoBehaviour {

    public PotaTween Animation;
    public BoardTile StartTile;
    public MeshRenderer HeadIcon;
    public GameEnums.BoardDirections BoardStartSide;
    public float DinossaurRotation;

    public Vector3 Position { get { return transform.position; } set { transform.position = value; } }
    public Animator Animator { get { return GetComponentInChildren<Animator>(); } }

    private Vector3 _bushesScale;


	void Start ()
    {
        _bushesScale = transform.localScale;
	}
	
	void Update ()
    {
		
	}

    public void ShowHeadIconBySeconds(float seconds, Material headIcon)
    {
        Animator.SetTrigger("Shake");
        HeadIcon.material = headIcon;

        Invoke("EnableHeadIcon", 1);
        Invoke("DisableHeadIcon", seconds);
    }

    private void EnableHeadIcon()
    {
        HeadIcon.gameObject.SetActive(true);
        HeadIcon.GetComponent<PotaTween>().SetEaseEquation(Ease.Equation.OutBack).Play();
    }

    private void DisableHeadIcon()
    {
        Animator.SetTrigger("DinoOut");
        HeadIcon.GetComponent<PotaTween>().SetEaseEquation(Ease.Equation.InBack).Reverse(() => HeadIcon.gameObject.SetActive(false));
    }
}
