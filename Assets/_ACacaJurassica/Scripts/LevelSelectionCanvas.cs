﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionCanvas : MonoBehaviour {

    public static LevelSelectionCanvas Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LevelSelectionCanvas>();

            return _instance;
        }
    }
    private static LevelSelectionCanvas _instance;

    public GameObject Container;
    public GameObject ContainerExperient;
    public GameObject ContainerProfessional;

    public Transform LevelList;
    public LevelButton LevelButtonPrefab;
    public Sprite[] LevelUnlockedSprites;
    public Button LeftArrow;
    public Button RightArrow;
    public Button BackButton;

    public List<LevelButton> Levels { get; set; }
    public List<LevelButton> LevelsExperient { get; set; }
    public List<LevelButton> LevelsProfessional { get; set; }
    public LevelSelection.LevelItem[] LevelsItems { get; set; }
    public bool InitializedExperient { get; set; }
    public bool InitializedProfessional { get; set; }

    private PotaTween _levelListTranslate;
    private float _levelListModifier = 535f;
    private int _numberOfLevels = 19;
    private int _currentLevel = 0;

    public void Initialize(GameEnums.GameType gameMode)
    {
        ContainerExperient.SetActive(gameMode == GameEnums.GameType.Experient);
        ContainerProfessional.SetActive(gameMode == GameEnums.GameType.Professional);

        if (gameMode == GameEnums.GameType.Experient)
            InitializeExperient();

        if (gameMode == GameEnums.GameType.Professional)
            InitializeProfessional();

        for (int i = 0; i < Levels.Count; i++)
        {
            Levels[i].Initialize(gameMode, LevelsItems[i].Unlocked, LevelsItems[i].StarsUnlocked);

            if (i == _currentLevel || i == _currentLevel + 1 || i == _currentLevel + 2)
             continue;

            Levels[i].gameObject.SetActive(false);
        }

        LevelList.localPosition = new Vector3(LevelList.localPosition.x - (_levelListModifier * _currentLevel), 0, 0);
        _levelListTranslate = PotaTween.Create(LevelList.gameObject).SetDuration(0.5f);

        LeftArrow.onClick.AddListener(() =>
        {
            Vector3 from = new Vector3(LevelList.localPosition.x, 0, 0);
            Vector3 to = new Vector3(LevelList.localPosition.x + _levelListModifier, 0, 0);
            int rightLevel = _currentLevel + 2;
            int leftLevel = _currentLevel - 1;

            _levelListTranslate.SetPosition(from, to, true).Play(UnlockButtons);

            Levels[rightLevel].Animations.Play("FadeOut", () => Levels[rightLevel].gameObject.SetActive(false));
            Levels[leftLevel].gameObject.SetActive(true);
            Levels[leftLevel].Animations.Play("FadeIn");

            _currentLevel--;
            LockButtons();
        });

        RightArrow.onClick.AddListener(() =>
        {
            Vector3 from = new Vector3(LevelList.localPosition.x, 0, 0);
            Vector3 to = new Vector3(LevelList.localPosition.x - _levelListModifier, 0, 0);
            int rightLevel = _currentLevel + 3;
            int leftLevel = _currentLevel;

            _levelListTranslate.SetPosition(from, to, true).Play(UnlockButtons);

            Levels[leftLevel].Animations.Play("FadeOut", () => Levels[leftLevel].gameObject.SetActive(false));
            Levels[rightLevel].gameObject.SetActive(true);
            Levels[rightLevel].Animations.Play("FadeIn");

            _currentLevel++;
            LockButtons();
        });

        BackButton.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

            OptionsController.Instance.LockButton();

            CloseLevelSelection();
            Invoke("OpenMainMenu", 0.7f);
        });

        LeftArrow.interactable = _currentLevel > 0;
        RightArrow.interactable = _currentLevel + 2 < Levels.Count - 1;
    }

    void Update()
    {

    }

    private void InitializeExperient()
    {
        _currentLevel = LevelSelectionExperient.CurrentLevelIndex;

        LevelsItems = LevelSelectionExperient.LevelItems;

        if (InitializedExperient)
        {
            Levels = LevelsExperient;
        }
        else
        {
            LevelsExperient = InstantiateLevelButtons(GameEnums.GameType.Experient);
            Levels = LevelsExperient;
            InitializedExperient = true;
        }
    }

    private void InitializeProfessional()
    {
        _currentLevel = LevelSelectionProfessional.CurrentLevelIndex;

        LevelsItems = LevelSelectionProfessional.LevelItems;

        if (InitializedProfessional)
        {
            Levels = LevelsProfessional;
        }
        else
        {
            LevelsProfessional = InstantiateLevelButtons(GameEnums.GameType.Professional);
            Levels = LevelsProfessional;
            InitializedProfessional = true;
        }
    }

    private List<LevelButton> InstantiateLevelButtons(GameEnums.GameType gameMode)
    {
        List<LevelButton> levelButtons = new List<LevelButton>();
        GameObject parent = gameMode == GameEnums.GameType.Experient ? ContainerExperient : ContainerProfessional;

        for (int i = 0; i < _numberOfLevels; i++)
        {
            levelButtons.Add(Instantiate(LevelButtonPrefab, parent.transform));
            levelButtons[i].transform.localPosition = new Vector3(_levelListModifier * i, 15, 0);
            levelButtons[i].UnlockedSprite.sprite = LevelUnlockedSprites[i];
            levelButtons[i].Index = i;
        }

        return levelButtons;
    }

    private void LockButtons()
    {
        LeftArrow.interactable = false;
        RightArrow.interactable = false;
        BackButton.interactable = false;

        for (int i = 0; i < Levels.Count; i++)
        {
            if(Levels[i].LevelUnlocked)
                Levels[i].Button.interactable = false;
        }
    }

    private void UnlockButtons()
    {
        if (_currentLevel != 0)
            LeftArrow.interactable = true;

        if (_currentLevel + 2 != Levels.Count - 1)
            RightArrow.interactable = true;

        BackButton.interactable = true;

        for (int i = 0; i < Levels.Count; i++)
        {
            if (Levels[i].LevelUnlocked)
                Levels[i].Button.interactable = true;
        }
    }

    public void OpenLevelSelection()
    {
        Levels[_currentLevel].Animations.Play("FadeIn");
        Levels[_currentLevel+1].Animations.Play("FadeIn");
        Levels[_currentLevel+2].Animations.Play("FadeIn");

        LeftArrow.GetComponent<PotaTween>().Play();
        RightArrow.GetComponent<PotaTween>().Play();
        BackButton.GetComponent<PotaTween>().Play();
    }

    public void CloseLevelSelection()
    {
        Levels[_currentLevel].Animations.Play("FadeOut");
        Levels[_currentLevel + 1].Animations.Play("FadeOut");
        Levels[_currentLevel + 2].Animations.Play("FadeOut");

        LeftArrow.GetComponent<PotaTween>().Reverse();
        RightArrow.GetComponent<PotaTween>().Reverse();
        BackButton.GetComponent<PotaTween>().Reverse();
    }

    private void OpenMainMenu()
    {
        MainMenu.Instance.OpenMainMenuFromSelection();
    }
}
