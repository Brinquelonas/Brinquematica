﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapAnimations : MonoBehaviour {

    public enum Animations
    {
        None,
        Break,
        Capture,
        Build
    }

    public Animations CurrentAnimation { get; set; }
    
    private Animator _animator;
    public Animator Animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();

            return _animator;
        }
    }

    public bool PlayCapture;
    public bool PlayBreak;
    public bool PlayBuild;

    public PotaTween BuildAnimation;
    public PotaTween CaptureAnimation;
    public PotaTween BreakAnimation;

    public ParticleSystem BuildParticles;
    public ParticleSystem CaptureParticles;
    public ParticleSystem BreakParticles;

    public Transform DinosaurBone;
    public Transform DinosaurPosition;

    public void PlayAnimation(Animations animation)
    {
        /*if (CurrentAnimation == animation)
            return;*/

        CurrentAnimation = animation;

        if (animation == Animations.Break)
            Animator.SetTrigger("Break");

        if (animation == Animations.Capture)
            Animator.SetTrigger("Capture");

        if (animation == Animations.Build)
            Animator.SetTrigger("Build");
    }

    public virtual void Update()
    {
        if (PlayBuild)
        {
            //PlayAnimation(Animations.Build);
            BuildTrap();
            PlayBuild = false;
        }

        if (PlayBreak)
        {
            //PlayAnimation(Animations.Break);
            BreakTrap();
            PlayBreak = false;
        }

        if (PlayCapture)
        {
            //PlayAnimation(Animations.Capture);
            CaptureDino();
            PlayCapture = false;
        }
    }

    public virtual void BuildTrap()
    {

    }

    public virtual void BreakTrap()
    {

    }

    public virtual void CaptureDino()
    {

    }
}
