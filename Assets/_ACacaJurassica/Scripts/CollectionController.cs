﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionController : MonoBehaviour {

    public static CollectionController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CollectionController>();

            return _instance;
        }
    }
    private static CollectionController _instance;

    [Serializable]
    public struct EggConfiguration
    {
        [Header("Configuration")]
        public int DinoIndex;
        public GameEnums.DinoColor DinoColor;
        public GameEnums.DinoType DinoType;

        [Header("References - Eggs")]
        public Sprite EggShell;
        public Sprite EggShellCracked;
        public Sprite EggShellBroken;
        public Color EggColor;

        [Header("References - Dinos")]
        public Sprite DinosaurIcon;
        public Material[] DinosaurMaterials;
        public GameObject DinosaurModel;
    }

    public EggConfiguration[] Eggs;

    public static List<CollectionDinosaur> Dinosaurs;
    public static int NumberOfEggs = 20;
    public static bool Initialized = false;
    public static bool NewDinoUnlocked = false;

    void Start ()
    {
        if (!Initialized)
            Initialize();
    }
	
	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            //UpdateCollectionItem(5, 2);
        }
    }

    public static void Initialize()
    {
        if (!Initialized)
        {
            Dinosaurs = new List<CollectionDinosaur>();

            for (int i = 0; i < NumberOfEggs; i++)
            {
                Dinosaurs.Add(new CollectionDinosaur { DinoIndex = i, Captured = false, CrackedShell = false, NumberOfCaptures = 0, NumberOfCapturesToCrack = 0, NumberOfCapturesToUnlock = 0 });
                int numberOfCaptures = 0;
                bool captured = false;
                string name = "";

                GameSaveController.Instance.GetDinossaurData(i, out numberOfCaptures, out captured, out name);
                Dinosaurs[i].NumberOfCaptures = numberOfCaptures;
                Dinosaurs[i].Captured = captured;
                Dinosaurs[i].Name = name;
            }
            
            //Blues
            Dinosaurs[0].NumberOfCapturesToUnlock = 30; Dinosaurs[0].NumberOfCapturesToCrack = 15;
            Dinosaurs[1].NumberOfCapturesToUnlock = 20; Dinosaurs[1].NumberOfCapturesToCrack = 10;
            Dinosaurs[2].NumberOfCapturesToUnlock = 30; Dinosaurs[2].NumberOfCapturesToCrack = 15;
            Dinosaurs[3].NumberOfCapturesToUnlock = 20; Dinosaurs[3].NumberOfCapturesToCrack = 10;

            //Greens
            Dinosaurs[4].NumberOfCapturesToUnlock = 3; Dinosaurs[4].NumberOfCapturesToCrack = 2;
            Dinosaurs[5].NumberOfCapturesToUnlock = 20; Dinosaurs[5].NumberOfCapturesToCrack = 10;
            Dinosaurs[6].NumberOfCapturesToUnlock = 30; Dinosaurs[6].NumberOfCapturesToCrack = 15;
            Dinosaurs[7].NumberOfCapturesToUnlock = 20; Dinosaurs[7].NumberOfCapturesToCrack = 10;

            //Oranges
            Dinosaurs[8].NumberOfCapturesToUnlock = 30; Dinosaurs[8].NumberOfCapturesToCrack = 15;
            Dinosaurs[9].NumberOfCapturesToUnlock = 20; Dinosaurs[9].NumberOfCapturesToCrack = 10;
            Dinosaurs[10].NumberOfCapturesToUnlock = 30; Dinosaurs[10].NumberOfCapturesToCrack = 15;
            Dinosaurs[11].NumberOfCapturesToUnlock = 20; Dinosaurs[11].NumberOfCapturesToCrack = 10;

            //Blacks
            Dinosaurs[12].NumberOfCapturesToUnlock = 20; Dinosaurs[12].NumberOfCapturesToCrack = 10;
            Dinosaurs[13].NumberOfCapturesToUnlock = 10; Dinosaurs[13].NumberOfCapturesToCrack = 5;
            Dinosaurs[14].NumberOfCapturesToUnlock = 20; Dinosaurs[14].NumberOfCapturesToCrack = 10;
            Dinosaurs[15].NumberOfCapturesToUnlock = 10; Dinosaurs[15].NumberOfCapturesToCrack = 5;
            
            //Rares
            Dinosaurs[16].NumberOfCapturesToUnlock = 10; Dinosaurs[16].NumberOfCapturesToCrack = 5;
            Dinosaurs[17].NumberOfCapturesToUnlock = 10; Dinosaurs[17].NumberOfCapturesToCrack = 5;
            Dinosaurs[18].NumberOfCapturesToUnlock = 5; Dinosaurs[18].NumberOfCapturesToCrack = 3;
            Dinosaurs[19].NumberOfCapturesToUnlock = 5; Dinosaurs[19].NumberOfCapturesToCrack = 3;

            UpdateCollection();
            Initialized = true;
        }
    }

    private static void UpdateCollection()
    {
        for (int i = 0; i < Dinosaurs.Count; i++)
        {
            if (Dinosaurs[i].NumberOfCaptures >= Dinosaurs[i].NumberOfCapturesToCrack)
            {
                Dinosaurs[i].CrackedShell = true;
            }

            if (Dinosaurs[i].NumberOfCaptures >= Dinosaurs[i].NumberOfCapturesToUnlock)
            {
                Dinosaurs[i].NumberOfCaptures = Dinosaurs[i].NumberOfCapturesToUnlock;
                Dinosaurs[i].Captured = true;
            }
        }
    }

    public static void UpdateCollectionItem(int dinoIndex, int numberOfCaptures)
    {
        for (int i = 0; i < Dinosaurs.Count; i++)
        {
            if (Dinosaurs[i].Captured)
                continue;

            if (Dinosaurs[i].DinoIndex == dinoIndex)
            {
                Dinosaurs[i].NumberOfCaptures += numberOfCaptures;

                if (Dinosaurs[i].NumberOfCaptures >= Dinosaurs[i].NumberOfCapturesToCrack)
                {
                    Dinosaurs[i].CrackedShell = true;
                }

                if (Dinosaurs[i].NumberOfCaptures >= Dinosaurs[i].NumberOfCapturesToUnlock)
                {
                    Dinosaurs[i].NumberOfCaptures = Dinosaurs[i].NumberOfCapturesToUnlock;
                    Dinosaurs[i].Captured = true;

                    NewDinoUnlocked = true;
                }

                GameSaveController.Instance.SetDinosaurData(Dinosaurs[i].DinoIndex, Dinosaurs[i].NumberOfCaptures, Dinosaurs[i].Captured);
            }
        }
    }
}
