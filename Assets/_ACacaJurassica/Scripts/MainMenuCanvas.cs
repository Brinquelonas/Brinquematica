﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvas : MonoBehaviour {

    public GameObject Container;
    public GameObject RulesContainer;
    public PotaTween RulesLogo;
    public PotaTween RulesText1;
    public PotaTween RulesText2;
    public PotaTween RulesText3;

    public PotaTween Fader;
    public PotaTween Margin;
    public PotaTween[] Buttons;
    public TweenAnimations Logo;
    public TweenAnimations CompanyLogo;

    public Action RulesCallback { get; set; }
    public Action CompanyLogoCallback { get; set; }

    void Start ()
    {

    }

    void Update ()
    {
		
	}

    public void PlayCompanyLogo(Action callback)
    {
        CompanyLogoCallback = callback;

        CompanyLogo.Play(0, () =>
        {
            CompanyLogo.Animations[1].gameObject.SetActive(true);
            CompanyLogo.Play(1, () =>
            {
                SfxController.Instance.Play(SfxController.Instance.Sparkle);
                Invoke("ReverseCompanyLogo", 2f);
            });
        });
    }

    private void ReverseCompanyLogo()
    {
        CompanyLogo.Play(2, () =>
        {
            if (CompanyLogoCallback != null)
                CompanyLogoCallback();
        });
    }

    public void PlayRules(Action callback = null)
    {
        RulesCallback = callback;
        RulesContainer.SetActive(true);

        RulesLogo.Play();
        RulesText1.Play();
        RulesText2.Play();

        Invoke("ReverseRulesText", 7.5f);
    }

    private void ReverseRulesText()
    {
        RulesText1.Reverse(() =>
        {
            RulesText3.Play();
            Invoke("CallbackRules", 7.5f);
        });

        RulesText2.Reverse();
    }

    private void CallbackRules()
    {
        RulesText3.Reverse();
        RulesLogo.Reverse(() =>
        {
            if (RulesCallback != null)
                RulesCallback();
        });
    }

    public void PlayLogoFirstTime(Action callback = null)
    {
        SfxController.Instance.Play(SfxController.Instance.Drums);
        Logo.Animations[0].SetDelay(2).Play(() =>
        {
            Logo.Animations[1].SetDelay(2.5f).Play();
            Fader.SetDelay(2.5f).Play(() =>
            {
                callback();
            });
        });

        Margin.SetDelay(2).Play();
    }

    public void PlayMarginAndFader(Action callback = null)
    {
        Margin.SetDelay(0).Play();
        Fader.SetDelay(0).Play(callback);
    }

    public void ReverseMarginAndFader(Action callback = null)
    {
        Margin.SetDelay(0).Reverse();
        Fader.SetDelay(0).Reverse(callback);
    }

    public void PlayButtonsAnimation(Action callback = null)
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].SetEaseEquation(Ease.Equation.OutBack).Play(callback);
        }
    }

    public void ReverseButtonsAnimation(Action callback = null)
    {
        for (int i = 0; i < Buttons.Length; i++)
        {
            Buttons[i].SetEaseEquation(Ease.Equation.InBack).Reverse(callback);
        }
    }
}
