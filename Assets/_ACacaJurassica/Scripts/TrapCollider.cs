﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapCollider : MonoBehaviour {

    /*public TrapAnimations Animations;
    public PotaTween OpenAnimation;
    public PotaTween CloseAnimation;
    public ParticleSystem Particles;*/

    public event Action<Collider> TriggerEnter;
    public event Action<Collider> TriggerExit;

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    /*public virtual void Open()
    {
        OpenAnimation.Play();
        Particles.Play();
    }

    public virtual void Close()
    {
        CloseAnimation.Play();
        Particles.Play();
    }

    public virtual void Capture()
    {
        Animations.PlayAnimation(TrapAnimations.Animations.Capture);
    }

    public virtual void Break()
    {
        Animations.PlayAnimation(TrapAnimations.Animations.Break);
        Particles.Play();
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if(TriggerEnter != null)
            TriggerEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if(TriggerExit != null)
            TriggerExit(other);
    }
}
