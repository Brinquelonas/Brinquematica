﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelCompletedCanvas : MonoBehaviour {

    public static LevelCompletedCanvas Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LevelCompletedCanvas>();

            return _instance;
        }
    }
    private static LevelCompletedCanvas _instance;

    [Header("Animations")]
    public Transform Container;
    public PotaTween Fader;
    public PotaTween Points;
    public PotaTween[] Stars;
    public PotaTween[] StarsBack;
    public PotaTween[] Buttons;
    public PotaTween CollectionGlow;

    [Header("References")]
    public Text PointsText;
    public Button PlayAgain;
    public Button NextLevel;
    public Button Collection;
    public Button Exit;

    private float _summedPoints;
    private float _countedPoints;
    private float _countModifier;
    private float[] _starsPoints;
    private bool[] _starsCounted;

    public Action FinishedCounting { get; set; }

    void Start ()
    {
        PlayAgain.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

            AudioController.Instance.Stop();
            ReloadGameplayScene();
        });

        NextLevel.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

            AudioController.Instance.Stop();
            LoadLevelSelection();
        });

        Collection.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxEggClick);

            AudioController.Instance.Stop();
            LoadCollection();
        });

        Exit.onClick.RemoveAllListeners();
        Exit.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

            AudioController.Instance.Stop();
            Close(() => SceneManager.LoadScene("MainMenu"));
        });
    }
	
	void Update ()
    {
		
	}

    public void Open()
    {
        Container.gameObject.SetActive(true);

        Fader.InitialState();
        Points.InitialState();

        StarsBack[0].InitialState();
        StarsBack[1].InitialState();
        StarsBack[2].InitialState();

        Stars[0].InitialState();
        Stars[1].InitialState();
        Stars[2].InitialState();

        Buttons[0].InitialState();
        Buttons[1].InitialState();
        Buttons[2].InitialState();

        FinishedCounting += () =>
        {
            Buttons[0].Play();
            Buttons[1].Play();
            Buttons[2].Play(() =>
            {
                PlayAgain.interactable = true;
                NextLevel.interactable = true;
                Collection.interactable = true;

                Invoke("PlayCollectionGlow", 0.25f);
            });
        };

        Fader.Play(() =>
        {
            Points.Play(() => CountPoints());

            StarsBack[0].Play();
            StarsBack[1].Play();
            StarsBack[2].Play();
        });

        Exit.onClick.RemoveAllListeners();
        Exit.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

            AudioController.Instance.Stop();
            Close(() => SceneManager.LoadScene("MainMenu"));
        });
    }

    private void Close(Action callback)
    {
        Points.SetEaseEquation(Ease.Equation.InBack).Reverse();

        for (int i = 0; i < 3; i++)
        {
            if (_starsCounted[i])
            {
                StarsBack[i].gameObject.SetActive(false);
                Stars[i].SetEaseEquation(Ease.Equation.InBack).Reverse();
            }
            else
            {
                Stars[i].gameObject.SetActive(false);
                StarsBack[i].SetEaseEquation(Ease.Equation.InBack).Reverse();
            }

            Buttons[i].SetEaseEquation(Ease.Equation.InBack).Reverse();
        }

        if (CollectionGlow.gameObject.activeSelf)
            CollectionGlow.Reverse();

        GameCanvas.Instance.Margin.Reverse();
        Fader.SetAlpha(0.5f, 1).SetDuration(0.65f).Play(callback);
    }

    private void CountPoints()
    {
        _summedPoints = LevelCompleted.Instance.SummedPoints;
        _countModifier = (_summedPoints / 200) < 1? 1 : Mathf.CeilToInt(_summedPoints / 200);
        _countedPoints = 0;

        _starsCounted = new bool[3];
        _starsPoints = new float[3];
        _starsPoints[0] = LevelCompleted.Instance.Star1Points;
        _starsPoints[1] = LevelCompleted.Instance.Star2Points;
        _starsPoints[2] = LevelCompleted.Instance.Star3Points;

        Invoke("ContinueCounting", 0.015f);
    }



    private void ContinueCounting()
    {
        _countedPoints += _countModifier;

        if(_countedPoints >= _summedPoints)
        {
            _countedPoints = _summedPoints;
            PointsText.text = _countedPoints.ToString("0000");

            for (int i = 0; i < 3; i++)
            {
                print(_countedPoints + " : " + _starsPoints[i]);
                if (!_starsCounted[i] && _countedPoints >= _starsPoints[i])
                {
                    Stars[i].Play();
                    _starsCounted[i] = true;

                    SfxController.Instance.Play(SfxController.Instance.SfxStar);
                }
            }

            if (FinishedCounting != null)
                FinishedCounting();

            return;
        }

        PointsText.text = _countedPoints.ToString("0000");

        for (int i = 0; i < 3; i++)
        {
            if(!_starsCounted[i] && _countedPoints >= _starsPoints[i])
            {
                Stars[i].Play();
                _starsCounted[i] = true;
            }
        }

        Invoke("ContinueCounting", 0.015f);
    }

    private void PlayCollectionGlow()
    {
        if (CollectionController.NewDinoUnlocked)
        {
            CollectionGlow.gameObject.SetActive(true);
            CollectionGlow.Play();

            CollectionController.NewDinoUnlocked = false;
        }
    }
    
    private void ReloadGameplayScene()
    {
        Close(() => SceneManager.LoadScene(SceneManager.GetActiveScene().name));
    }

    private void LoadLevelSelection()
    {
        MainMenu.DirectToLevelSelection = true;
        Close(() => SceneManager.LoadScene("MainMenu"));
    }

    private void LoadCollection()
    {
        Close(() => SceneManager.LoadScene("Collection"));
    }
}