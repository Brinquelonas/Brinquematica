﻿using UnityEngine;
using UnityEngine.UI;
using EasyAR;
using System.Linq;

public class GameTutorial : MonoBehaviour {

    public ImageTrackerBehaviour BoardTracker;
    public ImageTrackerBehaviour TrapTracker;
    public ImageTargetBehaviour BoardTarget;
    public ImageTargetBehaviour TrapTarget;
    public Button LoadTrapButton;
    public PotaTween LoadTrapAnimation;

    public Dinossaur DinoModel;
    public Bushes Bush;
    public BoardTile[] BoardTiles;
    public PotaTween DinoHeadIcon;
    public PotaTween[] BoardTilesMarkers;

    public PotaTween Fader;
    public PotaTween Page0;
    public PotaTween Page1;
    public PotaTween Page2;
    public PotaTween Page3;
    public PotaTween Page4;
    public PotaTween Page5;
    public PotaTween Page6;
    public Button Page0Ok;
    public Button Page1Ok;
    public Button Page2Ok;
    public Button Page3Ok;
    public Button Page4Ok;
    public Button Page5Ok;
    public Button Page6Ok;
    public Text Timer;

    private bool _pauseTimer;
    private float _gameTimer;
    private int _pageIndex = 0;

    private void Start()
    {
        _gameTimer = 60;
        _pauseTimer = true;
    }

    public void Initiate()
    {
        BoardTracker.StopTrack();
        TrapTracker.StopTrack();

        BoardTilesMarkers[0].InitialState();
        BoardTilesMarkers[1].InitialState();
        BoardTilesMarkers[2].InitialState();

        DinoHeadIcon.gameObject.SetActive(false);
        DinoModel.gameObject.SetActive(false);

        _gameTimer = 60;
        _pauseTimer = true;

        RegisterEvents();

        Invoke("OpenPage0", 2f);
	}

    void RegisterEvents()
    {
        Page0Ok.onClick.AddListener(() =>
        {
            ClosePage0();
        });

        Page1Ok.onClick.AddListener(() =>
        {
            ClosePage1();
        });

        Page2Ok.onClick.AddListener(() =>
        {
            ClosePage2();
        });

        Page3Ok.onClick.AddListener(() =>
        {
            ClosePage3();
        });

        Page4Ok.onClick.AddListener(() =>
        {
            ClosePage4();
        });

        Page5Ok.onClick.AddListener(() =>
        {
            ClosePage5();
        });

        Page6Ok.onClick.AddListener(() =>
        {
            ClosePage6();
        });

        BoardTarget.OnTargetFound.AddListener(() =>
        {
            Invoke("OpenPage2", 1);
        });

        LoadTrapButton.onClick.AddListener(() =>
        {
            BoardTracker.StopTrack();
            TrapTracker.StartTrack();
            LoadTrapButton.interactable = false;
        });

        TrapTarget.OnTargetFound.AddListener(() =>
        {
            BoardTracker.StartTrack();
            TrapTracker.StopTrack();

            Invoke("OpenPage5", 2f);
        });

        TutorialTrapController.Instance.DinoCaptured += () =>
        {
            Invoke("OpenPage6", 1);
        };
    }
	
	void Update ()
    {
        if (_pauseTimer)
            return;

        _gameTimer -= Time.deltaTime;

        Timer.text = Mathf.Floor(_gameTimer / 60).ToString("00") + ":" + Mathf.Floor(_gameTimer % 60).ToString("00");

        if (_gameTimer <= 0.0f)
        {
            Timer.text = "00:00";
            _pauseTimer = true;
        }
    }

    void OpenPage0()
    {
        if (_pageIndex > 0)
            return;

        _pageIndex++;

        Fader.Play();

        Page0.gameObject.SetActive(true);
        Page0.Play();
    }

    void ClosePage0()
    {
        Page0.Reverse(() =>
        {
            OpenPage1();
        });
    }

    void OpenPage1()
    {
        if (_pageIndex > 1)
            return;

        _pageIndex++;

        Page1.gameObject.SetActive(true);
        Page1.Play();
    }

    void ClosePage1()
    {
        Fader.Reverse();
        Page1.Reverse(() =>
        {
            BoardTracker.StartTrack();
        });
    }

    void OpenPage2()
    {
        if (_pageIndex > 2)
            return;

        _pageIndex++;

        _pauseTimer = false;

        Fader.Play();
        Page2.gameObject.SetActive(true);
        Page2.Play(() => _pauseTimer = true);
    }

    void ClosePage2()
    {
        Fader.Reverse();
        Page2.Reverse(() =>
        {
            _pauseTimer = false;

            DinoHeadIcon.gameObject.SetActive(true);
            DinoHeadIcon.Play(() =>
            {
                BoardTilesMarkers[0].Play(() =>
                {
                    BoardTilesMarkers[1].Play(() =>
                    {
                        BoardTilesMarkers[2].Play(() =>
                        {
                            Invoke("OpenPage3", 1);
                        });
                    });
                });
            });
        });
    }

    void OpenPage3()
    {
        if (_pageIndex > 3)
            return;

        _pageIndex++;

        Fader.Play();

        Page3.gameObject.SetActive(true);
        Page3.Play(() => _pauseTimer = true);
    }

    void ClosePage3()
    {
        Page3.Reverse(() =>
        {
            Invoke("OpenPage4", 0.5f);
        });
    }

    void OpenPage4()
    {
        if (_pageIndex > 4)
            return;

        _pageIndex++;

        Page4.gameObject.SetActive(true);
        Page4.Play();
    }

    void ClosePage4()
    {
        Fader.Reverse();
        Page4.Reverse(() =>
        {
            LoadTrapButton.gameObject.SetActive(true);
            LoadTrapAnimation.Play();
        });
    }

    void OpenPage5()
    {
        if (_pageIndex > 5)
            return;

        _pageIndex++;

        Fader.Play();

        Page5.gameObject.SetActive(true);
        Page5.Play();
    }

    void ClosePage5()
    {
        Fader.Reverse();
        Page5.Reverse(() =>
        {
            DinoHeadIcon.SetDelay(2f).Reverse();
            BoardTilesMarkers[0].SetDelay(2f).Reverse();
            BoardTilesMarkers[1].SetDelay(2f).Reverse();
            BoardTilesMarkers[2].SetDelay(2f).Reverse(() =>
            {
                _pauseTimer = false;

                DinoModel.gameObject.SetActive(true);
                DinoModel.SetBoardPathOnTutorial(BoardTiles.ToList(), Bush);
                DinoModel.StartPathWalk();
            });
        });
    }

    void OpenPage6()
    {
        if (_pageIndex > 6)
            return;

        _pageIndex++;

        _pauseTimer = true;

        Fader.Play();

        Page6.gameObject.SetActive(true);
        Page6.Play();
    }

    void ClosePage6()
    {
        Fader.Reverse();
        Page6.Reverse(() =>
        {
            Invoke("OpenLevelCompleted", 1);
        });
    }

    void OpenLevelCompleted()
    {
        TutorialLevelCompleted.Instance.Open();
    }
}
