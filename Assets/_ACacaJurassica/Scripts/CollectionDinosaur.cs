﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectionDinosaur : MonoBehaviour {

    public int DinoIndex { get; set; }
    public int NumberOfCaptures { get; set; }
    public int NumberOfCapturesToCrack { get; set; }
    public int NumberOfCapturesToUnlock { get; set; }
    public bool CrackedShell { get; set; }
    public bool Captured { get; set; }
    public string Name { get; set; }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}
}
