﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompleted : MonoBehaviour {

    public static LevelCompleted Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LevelCompleted>();

            return _instance;
        }
    }
    private static LevelCompleted _instance;

    public float SummedPoints { get; set; }
    public float Star1Points { get; set; }
    public float Star2Points { get; set; }
    public float Star3Points { get; set; }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void SetLevelCompletedValues()
    {
        SummedPoints = GameController.Instance.Points;
        Star1Points = GameController.Instance.CurrentLevelVariables.Star1Points;
        Star2Points = GameController.Instance.CurrentLevelVariables.Star2Points;
        Star3Points = GameController.Instance.CurrentLevelVariables.Star3Points;

        int numberOfStars = 0;

        if(MainMenu.GameModeSelected == GameEnums.GameType.Experient)
        {
            LevelSelectionExperient.CurrentLevelItem.Played = true;

            if(SummedPoints > LevelSelectionExperient.CurrentLevelItem.Points)
                LevelSelectionExperient.CurrentLevelItem.Points = (int)SummedPoints;

            if (!LevelSelectionExperient.CurrentLevelItem.StarsUnlocked[0] && SummedPoints >= Star1Points)
            {
                LevelSelectionExperient.CurrentLevelItem.StarsUnlocked[0] = true;
                LevelSelectionExperient.LevelItems[LevelSelectionExperient.NextLevelIndex].Unlocked = true;
                GameSaveController.Instance.CurrentLevelExperient = LevelSelectionExperient.NextLevelIndex;
                numberOfStars++;
            }

            if (!LevelSelectionExperient.CurrentLevelItem.StarsUnlocked[1] && SummedPoints >= Star2Points)
            {
                LevelSelectionExperient.CurrentLevelItem.StarsUnlocked[1] = true;
                numberOfStars++;
            }

            if (!LevelSelectionExperient.CurrentLevelItem.StarsUnlocked[2] && SummedPoints >= Star3Points)
            {
                LevelSelectionExperient.CurrentLevelItem.StarsUnlocked[2] = true;
                numberOfStars++;
            }
            
            if(numberOfStars > LevelSelectionExperient.CurrentLevelItem.Stars)
                GameSaveController.Instance.SetLevelData(GameEnums.GameType.Experient, LevelSelectionExperient.CurrentLevelIndex, numberOfStars);
        }

        if (MainMenu.GameModeSelected == GameEnums.GameType.Professional)
        {
            LevelSelectionProfessional.CurrentLevelItem.Played = true;

            if (SummedPoints > LevelSelectionProfessional.CurrentLevelItem.Points)
                LevelSelectionProfessional.CurrentLevelItem.Points = (int)SummedPoints;

            if (!LevelSelectionProfessional.CurrentLevelItem.StarsUnlocked[0] && SummedPoints >= Star1Points)
            {
                LevelSelectionProfessional.CurrentLevelItem.StarsUnlocked[0] = true;
                LevelSelectionProfessional.LevelItems[LevelSelectionProfessional.NextLevelIndex].Unlocked = true;
                GameSaveController.Instance.CurrentLevelExperient = LevelSelectionProfessional.NextLevelIndex;
                numberOfStars++;
            }

            if (!LevelSelectionProfessional.CurrentLevelItem.StarsUnlocked[1] && SummedPoints >= Star2Points)
            {
                LevelSelectionProfessional.CurrentLevelItem.StarsUnlocked[1] = true;
                numberOfStars++;
            }

            if (!LevelSelectionProfessional.CurrentLevelItem.StarsUnlocked[2] && SummedPoints >= Star3Points)
            {
                LevelSelectionProfessional.CurrentLevelItem.StarsUnlocked[2] = true;
                numberOfStars++;
            }

            if (numberOfStars > LevelSelectionProfessional.CurrentLevelItem.Stars)
                GameSaveController.Instance.SetLevelData(GameEnums.GameType.Professional, LevelSelectionProfessional.CurrentLevelIndex, numberOfStars);
        }
    }
}
