﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSaveController : MonoBehaviour {

    public static GameSaveController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameSaveController>();

            return _instance;
        }
    }
    private static GameSaveController _instance;

	void Awake ()
    {
        if(_instance == null)
            DontDestroyOnLoad(gameObject);

        if(!PlayerPrefs.HasKey("GameHasSavedData"))
        {
            InitializeData();

            PlayerPrefs.SetInt("GameHasSavedData", 0);
            PlayerPrefs.Save();
        }
    }

    public bool DontShowWarningAgain
    {
        get { return PlayerPrefs.GetInt("DontShowWarningAgain") == 1; }
        set { PlayerPrefs.SetInt("DontShowWarningAgain", value? 1 : 0); }
    }

    public int CurrentLevelExperient
    {
        get { return PlayerPrefs.GetInt("CurrentLevelExperient"); }
        set { PlayerPrefs.SetInt("CurrentLevelExperient", value); }
    }

    public int CurrentLevelProfessional
    {
        get { return PlayerPrefs.GetInt("CurrentLevelProfessional"); }
        set { PlayerPrefs.SetInt("CurrentLevelProfessional", value); }
    }

    void Update ()
    {
		
	}

    public void InitializeData()
    {
        for (int i = 0; i < 19; i++)
        {
            PlayerPrefs.SetInt("Dinosaur" + i + "NumberOfCaptures", 0);
            PlayerPrefs.SetInt("Dinosaur" + i + "Captured", 0);
            PlayerPrefs.SetString("Dinosaur" + i + "Name", "");
        }

        for (int i = 0; i < 18; i++)
        {
            PlayerPrefs.SetInt("LevelExperient" + i + "Stars", 0);
            PlayerPrefs.SetInt("LevelProfessional" + i + "Stars", 0);
        }

        PlayerPrefs.SetInt("CurrentLevelExperient", 0);
        PlayerPrefs.SetInt("CurrentLevelProfessional", 0);
        PlayerPrefs.SetInt("SfxEnabled", 1);
        PlayerPrefs.SetInt("MusicEnabled", 1);
        PlayerPrefs.SetInt("DontShowWarningAgain", 0);
    }

    public void GetDinossaurData(int index, out int numberOfCaptures, out bool captured, out string name)
    {
        numberOfCaptures = PlayerPrefs.GetInt("Dinosaur" + index + "NumberOfCaptures");
        captured = PlayerPrefs.GetInt("Dinosaur" + index + "Captured") == 1;
        name = PlayerPrefs.GetString("Dinosaur" + index + "Name");
    }

    public void SetDinosaurData(int index, int numberOfCaptures, bool captured)
    {
        PlayerPrefs.SetInt("Dinosaur" + index + "NumberOfCaptures", numberOfCaptures);
        PlayerPrefs.SetInt("Dinosaur" + index + "Captured", captured? 1 : 0);
        PlayerPrefs.SetString("Dinosaur" + index + "Name", name);
        PlayerPrefs.Save();
    }

    public void SetDinosaurName(int index, string name)
    {
        PlayerPrefs.SetString("Dinosaur" + index + "Name", name);
        PlayerPrefs.Save();
    }

    public void GetLevelData(GameEnums.GameType levelType, int levelIndex, out int stars)
    {
        stars = PlayerPrefs.GetInt((levelType == GameEnums.GameType.Experient ? "LevelExperient" : "LevelProfessional") + levelIndex + "Stars");
    }

    public void SetLevelData(GameEnums.GameType levelType, int levelIndex, int stars)
    {
        PlayerPrefs.SetInt((levelType == GameEnums.GameType.Experient ? "LevelExperient" : "LevelProfessional") + levelIndex + "Stars", stars);
    }
}
