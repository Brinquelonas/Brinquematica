﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {

    public GameTutorial ExperientTutorial;
    public GameTutorialProfessional ProfessionalTutorial;

    public enum TutorialState
    {
        Page0,
        Page1,
        Page2,
        Page3,
        Page4,
        Page5,
        Page6,
        Page7,
        Page8,
        Page9,
        None
    }

	void Start ()
    {
		if(MainMenu.GameModeSelected == GameEnums.GameType.Experient)
        {
            ExperientTutorial.Initiate();
        }

        if (MainMenu.GameModeSelected == GameEnums.GameType.Professional)
        {
            ProfessionalTutorial.Initiate();
        }
    }
	
	void Update ()
    {
		
	}
}
