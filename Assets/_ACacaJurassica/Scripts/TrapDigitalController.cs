﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDigitalController : MonoBehaviour {

    public static TrapDigitalController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<TrapDigitalController>();

            return _instance;
        }
    }
    private static TrapDigitalController _instance;

    public Transform TrapsContainer;
    public Transform TrapsTemplate;
    
	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void CreateTrapOnBoard(TrapDigital trap)
    {
        TrapDigital newTrap = Instantiate(trap, TrapsContainer);
        newTrap.transform.localPosition = TrapsTemplate.localPosition;
        newTrap.transform.localEulerAngles = TrapsTemplate.localEulerAngles;
        newTrap.transform.localScale = TrapsTemplate.localScale;

        newTrap.Animations.BuildTrap();
        //newTrap.TrapBase.Open();
    }

    public void UpdatePoints(float points)
    {
        GameController.Instance.Points += points;
        GameCanvas.Instance.UpdatePointsText(GameController.Instance.Points);
    }

    public void PlayCapturedText()
    {
        GameCanvas.Instance.Captured.Play(0);
        Invoke("ReverseCapturedText", 1.5f);
    }

    private void ReverseCapturedText()
    {
        GameCanvas.Instance.Captured.Play(1);
    }
}
