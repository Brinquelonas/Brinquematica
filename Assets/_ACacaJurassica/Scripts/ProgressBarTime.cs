﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ProgressBarTime : MonoBehaviour {

    [System.Serializable]
    public class ProgressBarEvent : UnityEvent { }

    public Image Track;
    public Image Bar;
    public float TimeToFill;
    public ProgressBarEvent OnStarted = new ProgressBarEvent();
    public ProgressBarEvent OnCompleted = new ProgressBarEvent();

    private PotaTween _barAnimation;
    private bool _started = false;

    void Start ()
    {
		
	}
	
	void Update ()
    {
        if (!_started)
            return;

        if (_barAnimation == null)
            return;

        Bar.fillAmount = _barAnimation.Float.Value;
	}

    public void StartProgressBar()
    {
        _started = true;

        if (_barAnimation == null)
            _barAnimation = PotaTween.Create(gameObject);

        _barAnimation.SetDuration(TimeToFill).SetFloat(0, 1).Play(OnProgressCompleted);

        OnProgressStarted();
    }

    private void OnProgressStarted()
    {
        OnStarted.Invoke();
    }

    private void OnProgressCompleted()
    {
        _started = false;

        OnCompleted.Invoke();
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }
}
