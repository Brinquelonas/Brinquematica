﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapBoxAnimations : TrapAnimations {

    void Start ()
    {
		
	}
	
	public override void Update()
    {
        base.Update();
	}

    public override void BuildTrap()
    {
        base.BuildTrap();

        BuildAnimation.Play();
        BuildParticles.Play();
    }

    public override void BreakTrap()
    {
        base.BreakTrap();

        PlayAnimation(Animations.Break);
        BreakAnimation.SetDelay(1).Play();
        BreakParticles.Play();
    }

    public override void CaptureDino()
    {
        base.CaptureDino();

        PlayAnimation(Animations.Capture);
        CaptureAnimation.SetDelay(1).Play();

        Invoke("CaptureParticlesDelay", 1);
    }

    private void CaptureParticlesDelay()
    {
        CaptureParticles.Play();
    }
}
