﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathWindow : MonoBehaviour {

    public PathWindowLayout Layout2Directions;
    public PathWindowLayout Layout3Directions;
    public PathWindowLayout Layout4Directions;
    public PathWindowLayout Layout5Directions;
    public MeshRenderer DinossaurHead;

    public PathWindowLayout CurrentLayout { get; set; }
    
    void Start ()
    {

    }
	
	void Update ()
    {

	}

    public void SetPathWindow(List<GameEnums.BoardDirections> directions)
    {
        GetLayoutForDirections(directions.Count);

        CurrentLayout.SetDirectionsToArrows(directions);
    }

    public void UpdateDinossaurHead(Material iconHead)
    {
        DinossaurHead.material = iconHead;
    }

    private void GetLayoutForDirections(int numberOfDirections)
    {
        if(CurrentLayout != null)
            CurrentLayout.gameObject.SetActive(false);

        switch (numberOfDirections)
        {
            case 2:
                CurrentLayout = Layout2Directions;
                break;
            case 3:
                CurrentLayout = Layout3Directions;
                break;
            case 4:
                CurrentLayout = Layout4Directions;
                break;
            case 5:
                CurrentLayout = Layout5Directions;
                break;
        }

        CurrentLayout.gameObject.SetActive(true);
    }
}
