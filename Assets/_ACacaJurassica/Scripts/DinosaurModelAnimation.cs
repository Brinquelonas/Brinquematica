﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinosaurModelAnimation : MonoBehaviour {

    public DinosaurAnimations Animation;
    public PotaTween ScaleAnimation;
    public AudioSource SourceVoice { get; set; }
    public AudioSource SourceRunning { get; set; }
    public AudioClip[] DinoVoice;

    public bool ClickBlock { get; set; }

    private AudioClip GetDinoVoice { get { return DinoVoice[Random.Range(0, DinoVoice.Length)]; } }

    void Start()
    {

    }

    void Update()
    {

    }

    void OnMouseDown()
    {
        if (ClickBlock)
            return;

        ClickBlock = true;
        Appear();

        Invoke("ClickUnblock", 1);
        Invoke("Running", 0.5f);
        Invoke("Idle", 4f);
    }

    public void JumpOffEgg(float seconds)
    {
        Invoke("JumpOffEgg", seconds);
    }

    public void JumpOffEgg()
    {
        gameObject.SetActive(true);
        ScaleAnimation.Play();
        Appear();

        Invoke("Running", 0.5f);
        Invoke("Idle", Random.Range(4f, 8f));
    }

    public void Appear()
    {
        Animation.PlayAnimation(DinosaurAnimations.Anims.Appear);
        SourceVoice.PlayOneShot(GetDinoVoice, 1);
    }

    private void Idle()
    {
        Animation.PlayAnimation(DinosaurAnimations.Anims.Idle);
        SourceRunning.Stop();
    }

    private void DinoGrowl()
    {
        SourceVoice.PlayOneShot(DinoVoice[3], 1);
    }

    private void DinoSmallGrowl()
    {
        SourceVoice.PlayOneShot(DinoVoice[1], 1);
    }

    private void Running()
    {
        Animation.PlayAnimation(DinosaurAnimations.Anims.Running);
        SourceRunning.Play();
    }

    private void ClickUnblock()
    {
        ClickBlock = false;
    }

    public void Reset()
    {
        Animation.PlayAnimation(DinosaurAnimations.Anims.None);
        ClickUnblock();
    }
}
