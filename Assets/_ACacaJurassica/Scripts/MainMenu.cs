﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public static MainMenu Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MainMenu>();

            return _instance;
        }
    }
    private static MainMenu _instance;

    public static GameEnums.GameType GameModeSelected;
    public static bool StartedGame;
    public static bool DirectToLevelSelection;

    public MainMenuCanvas Canvas;
    public Button CollectionButton;
    public Button PlayButton1;
    public Button PlayButton2;
    public Button PlayButton3;
    public Toggle WarningsCheckbox;

    public GameEnums.GameType GameMode { get; set; }

    private PotaTween[] _logoAnimations;
    private PotaTween _faderAnimations;

    void Start ()
    {
        OptionsController.Instance.LockButton();
        OptionsController.Instance.LeaveMatchButton.interactable = false;

        if (!StartedGame)
        {
            SfxController.SfxEnabled = true;
            AudioController.MusicEnabled = true;

            OpenMainMenuFirstTime();
            CollectionController.Initialize();
        }
        else
        {
            if (DirectToLevelSelection)
            {
                OpenDirectSelection();
                DirectToLevelSelection = false;
            }
            else
            {
                OpenMainMenu();
            }
        }

        WarningsCheckbox.onValueChanged.AddListener((value) =>
        {
            GameSaveController.Instance.DontShowWarningAgain = value;
        });

        PlayButton1.onClick.AddListener(() =>
        {
            GameMode = GameEnums.GameType.Beginner;
            GameModeSelected = GameMode;

            OptionsController.Instance.LockButton();

            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);
            Canvas.ReverseButtonsAnimation();
            Invoke("OpenDifficultySelection", 1.2f);
        });

        PlayButton2.onClick.AddListener(() =>
        {
            GameMode = GameEnums.GameType.Experient;
            GameModeSelected = GameMode;

            OptionsController.Instance.LockButton();

            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);
            Canvas.ReverseButtonsAnimation();
            Invoke("OpenLevelSelection", 1.2f);
        });

        PlayButton3.onClick.AddListener(() =>
        {
            GameMode = GameEnums.GameType.Professional;
            GameModeSelected = GameMode;

            OptionsController.Instance.LockButton();

            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);
            Canvas.ReverseButtonsAnimation();
            Invoke("OpenLevelSelection", 1.2f);
        });

        CollectionButton.onClick.AddListener(() =>
        {
            Canvas.ReverseMarginAndFader();
            Canvas.ReverseButtonsAnimation();

            SfxController.Instance.Play(SfxController.Instance.SfxEggClick);
            OptionsController.Instance.LockButton();

            Invoke("LoadCollection", 1.25f);
        });

        StartedGame = true;
    }
	
	void Update ()
    {

    }

    public void ChangeSceneGameplayBeginner()
    {
        Canvas.ReverseMarginAndFader();
        OptionsController.Instance.LockButton();

        Invoke("LoadGameplayBeginner", 1);
    }

    void LoadGameplayBeginner()
    {
        AudioController.Instance.Stop();
        SceneManager.LoadScene("Gameplay Beginner");
    }

    public void ChangeSceneGameplayProfessional()
    {
        Canvas.ReverseMarginAndFader();
        OptionsController.Instance.LockButton();

        Invoke("LoadGameplayProfessional", 1);
    }

    void LoadGameplayProfessional()
    {
        AudioController.Instance.Stop();
        SceneManager.LoadScene("Gameplay Professional");
    }

    public void ChangeSceneGameplayExperient()
    {
        Canvas.ReverseMarginAndFader();
        OptionsController.Instance.LockButton();

        Invoke("LoadGameplayExperient", 1);
    }

    void LoadGameplayExperient()
    {
        AudioController.Instance.Stop();
        SceneManager.LoadScene("Gameplay Experient");
    }

    public void ChangeSceneGameplayTutorial()
    {
        Canvas.ReverseMarginAndFader();
        OptionsController.Instance.LockButton();

        Invoke("LoadGameplayTutorial", 1);
    }

    void LoadGameplayTutorial()
    {
        AudioController.Instance.Stop();
        SceneManager.LoadScene("Gameplay Tutorial");
    }

    void LoadCollection()
    {
        AudioController.Instance.Stop();
        SceneManager.LoadScene("Collection");
    }

    public void OpenMainMenuFirstTime()
    {
        if(GameSaveController.Instance.DontShowWarningAgain)
        {
            Canvas.PlayCompanyLogo(() =>
            {
                Canvas.PlayLogoFirstTime(() =>
                {
                    AudioController.Instance.Play();

                    Canvas.Container.SetActive(true);
                    Canvas.PlayButtonsAnimation(() =>
                    {
                        OptionsController.Instance.UnlockButton();
                        PlayButton1.interactable = true;
                        PlayButton2.interactable = true;
                        PlayButton3.interactable = true;
                        CollectionButton.interactable = true;
                    });
                });
            });
        }
        else
        {
            Canvas.PlayCompanyLogo(() =>
            {
                Canvas.PlayRules(() =>
                {
                    Canvas.PlayLogoFirstTime(() =>
                    {
                        AudioController.Instance.Play();

                        Canvas.Container.SetActive(true);
                        Canvas.PlayButtonsAnimation(() =>
                        {
                            OptionsController.Instance.UnlockButton();
                            PlayButton1.interactable = true;
                            PlayButton2.interactable = true;
                            PlayButton3.interactable = true;
                            CollectionButton.interactable = true;
                        });
                    });
                });
            });
        }
    }

    public void OpenMainMenu()
    {
        Canvas.PlayMarginAndFader(() =>
        {
            AudioController.Instance.Play();

            Canvas.Container.SetActive(true);
            Canvas.PlayButtonsAnimation(() =>
            {
                OptionsController.Instance.UnlockButton();
                PlayButton1.interactable = true;
                PlayButton2.interactable = true;
                PlayButton3.interactable = true;
                CollectionButton.interactable = true;
            });
        });
    }

    public void OpenMainMenuFromSelection()
    {
        Canvas.Container.SetActive(true);
        Canvas.PlayButtonsAnimation(() =>
        {
            OptionsController.Instance.UnlockButton();
            PlayButton1.interactable = true;
            PlayButton2.interactable = true;
            PlayButton3.interactable = true;
            CollectionButton.interactable = true;
        });
    }

    public void OpenLevelSelection()
    {
        OptionsController.Instance.UnlockButton();

        Canvas.Container.SetActive(false);

        LevelSelectionCanvas.Instance.Initialize(GameMode);
        LevelSelectionCanvas.Instance.Container.SetActive(true);
        LevelSelectionCanvas.Instance.OpenLevelSelection();
    }

    public void OpenDifficultySelection()
    {
        OptionsController.Instance.UnlockButton();

        Canvas.Container.SetActive(false);
        
        DifficultySelection.Instance.OpenDifficultySelection();
    }

    public void OpenDirectSelection()
    {
        GameMode = GameModeSelected;

        Canvas.Container.SetActive(false);
        Canvas.PlayMarginAndFader(() =>
        {
            OpenLevelSelection();
        });
    }
}
