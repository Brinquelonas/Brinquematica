﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelection : MonoBehaviour {
    
    public struct LevelItem
    {
        public int Index;
        public bool Unlocked;
        public bool Played;

        public int Points;
        public int Stars;
        public bool[] StarsUnlocked;
    }

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}
}
