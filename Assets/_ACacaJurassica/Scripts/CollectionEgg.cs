﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectionEgg : MonoBehaviour {

    [Header("References")]
    public Button Button;
    public Image EggShell;
    public Image EggShellBroken;
    public Image DinosaurIcon;
    public Image BarCompletion;
    public Text BarText;
    public PotaTween EggAppear;
    public PotaTween EggShellGlow;
    public PotaTween EggShellBrokenGlow;

    public CollectionDinosaur Dinosaur;
    public GameObject DinosaurModel { get; set; }
    public Material[] DinosaurMaterials { get; set; }
    public Sprite EggShellCracked { get; set; }
    public PotaTween Glow { get; set; }
    public bool CrackedShell { get; set; }
    public bool DinoCaptured { get; set; }


    void Start ()
    {
        Button.onClick.AddListener(() =>
        {
            CollectionCanvas.Instance.ClickOnEgg(this);
        });
	}
	
	void Update ()
    {
		
	}

    public void Initialize(CollectionController.EggConfiguration egg, CollectionDinosaur dino)
    {
        Dinosaur = dino;

        EggShell.sprite = egg.EggShell;
        EggShellBroken.sprite = egg.EggShellBroken;
        EggShellCracked = egg.EggShellCracked;
        DinosaurIcon.sprite = egg.DinosaurIcon;
        DinosaurIcon.SetNativeSize();
        DinosaurModel = egg.DinosaurModel;
        DinosaurMaterials = egg.DinosaurMaterials;

        EggShellGlow.GetComponent<Image>().color = egg.EggColor;
        EggShellBrokenGlow.GetComponent<Image>().color = egg.EggColor;

        EggShellGlow.gameObject.SetActive(false);
        EggShellBrokenGlow.gameObject.SetActive(false);

        CrackedShell = dino.CrackedShell;
        DinoCaptured = dino.Captured;
    }

    public void SetEggCaptured()
    {
        BarText.text = "Capturado!";
        BarCompletion.fillAmount = 1;

        EggShell.gameObject.SetActive(false);
        EggShellBroken.gameObject.SetActive(true);
        Glow = EggShellBrokenGlow;

        if (DinosaurModel != null)
        {
            DinosaurModel model = DinosaurModel.GetComponentInChildren<DinosaurModel>();
            model.Initialize();

            model.BodyRender.material = DinosaurMaterials[0];
            model.HeadRender.material = DinosaurMaterials[0];
            model.SpikesRender.material = DinosaurMaterials[1];
            model.FinsRender.material = DinosaurMaterials[1];
            model.MembranesRender.material = DinosaurMaterials[1];
            model.EyesRender.material = DinosaurMaterials[2];
        }
    }

    public void SetEggNormal(int numOfCaptures, int numOfCapturesToUnlock)
    {
        BarText.text = numOfCaptures + " - " + numOfCapturesToUnlock;
        BarCompletion.fillAmount = ((float)numOfCaptures / (float)numOfCapturesToUnlock);
        Glow = EggShellGlow;

        if (CrackedShell)
            EggShell.sprite = EggShellCracked;

        if (DinosaurModel != null)
        {
            Renderer[] renders = DinosaurModel.GetComponentsInChildren<Renderer>();

            for (int i = 0; i < renders.Length; i++)
            {
                renders[i].material = CollectionCanvas.Instance.DinosaurLockedBlack;
            }
        }
    }

    public void PlayGlow()
    {
        Glow.gameObject.SetActive(true);
        Glow.Play();
    }

    public void ReverseGlow()
    {
        Glow.Reverse(() => Glow.gameObject.SetActive(false));
    }
}
