﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectionProfessional : MonoBehaviour {

    public static LevelSelectionProfessional Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LevelSelectionProfessional>();

            return _instance;
        }
    }
    private static LevelSelectionProfessional _instance;

    public static LevelSelection.LevelItem[] LevelItems;
    public GameLevelVariables[] LevelVariables;

    public static GameLevelVariables CurrentLevelVariables;
    public static LevelSelection.LevelItem CurrentLevelItem;
    public static int CurrentLevelIndex;
    public static int NextLevelIndex;

    void Start()
    {
        if (LevelItems == null)
        {
            LevelItems = new LevelSelection.LevelItem[LevelVariables.Length];

            for (int i = 0; i < LevelItems.Length; i++)
            {
                int stars = 0;
                GameSaveController.Instance.GetLevelData(GameEnums.GameType.Professional, i, out stars);

                LevelItems[i].Index = i;
                LevelItems[i].Unlocked = i <= GameSaveController.Instance.CurrentLevelExperient;
                LevelItems[i].Played = i <= GameSaveController.Instance.CurrentLevelExperient;
                LevelItems[i].Points = 0;
                LevelItems[i].Stars = stars;
                LevelItems[i].StarsUnlocked = new bool[] { stars >= 1, stars >= 2, stars >= 3 };
            }

            LevelItems[0].Unlocked = true;
        }
    }

    void Update()
    {

    }

    public void SetCurrentLevel(int index)
    {
        CurrentLevelItem = LevelItems[index];
        CurrentLevelVariables = LevelVariables[index];
        CurrentLevelIndex = index;

        if (index + 1 < LevelItems.Length)
            NextLevelIndex = index + 1;
    }
}
