﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputName : MonoBehaviour {

    public static InputName Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<InputName>();

            return _instance;
        }
    }
    private static InputName _instance;

    public InputField Input;
    public Text InputText;
    public Text FinalText;

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void SetFinalText()
    {
        FinalText.text = Input.text;
        Input.text = "";

        CollectionCanvas.Instance.CurrentDino.Name = FinalText.text;
        GameSaveController.Instance.SetDinosaurName(CollectionCanvas.Instance.CurrentDino.DinoIndex, CollectionCanvas.Instance.CurrentDino.Name);
    }

    public void SetDinoText(string dinoName)
    {
        FinalText.text = dinoName;
    }

    public void ResetInput()
    {
        InputText.text = "";
        Input.text = "";
        Input.textComponent.text = "";
    }
}
