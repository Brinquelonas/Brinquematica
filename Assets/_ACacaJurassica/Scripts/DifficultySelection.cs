﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultySelection : MonoBehaviour {

    public static DifficultySelection Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<DifficultySelection>();

            return _instance;
        }
    }
    private static DifficultySelection _instance;

    public Button[] Buttons = new Button[3];
    public Button BackButton;
    public GameObject Container;

    void Start()
    {
        InitializeButtons();

        BackButton.onClick.AddListener(() =>
        {
            CloseDifficultySelection();
            Invoke("OpenMainMenu", 0.7f);
        });
    }

    void InitializeButtons()
    {
        Buttons[0].onClick.AddListener(() =>
        {
            LevelSelectionBeginner.Instance.SetCurrentLevel(0);
            MainMenu.Instance.ChangeSceneGameplayBeginner();
        });

        Buttons[1].onClick.AddListener(() =>
        {
            LevelSelectionBeginner.Instance.SetCurrentLevel(1);
            MainMenu.Instance.ChangeSceneGameplayBeginner();
        });

        Buttons[2].onClick.AddListener(() =>
        {
            LevelSelectionBeginner.Instance.SetCurrentLevel(2);
            MainMenu.Instance.ChangeSceneGameplayBeginner();
        });
    }

    public void OpenDifficultySelection()
    {
        //Levels[_currentLevel].Animations.Play("FadeIn");
        //Levels[_currentLevel + 1].Animations.Play("FadeIn");
        //Levels[_currentLevel + 2].Animations.Play("FadeIn");
        Container.SetActive(true);
        Container.GetComponent<PotaTween>().Play();
        
        //BackButton.GetComponent<PotaTween>().Play();
    }

    public void CloseDifficultySelection()
    {
        //Levels[_currentLevel].Animations.Play("FadeOut");
        //Levels[_currentLevel + 1].Animations.Play("FadeOut");
        //Levels[_currentLevel + 2].Animations.Play("FadeOut");
        Container.GetComponent<PotaTween>().Reverse(() =>
        {
            Container.SetActive(false);
        });

        //BackButton.GetComponent<PotaTween>().Reverse();
    }

    private void OpenMainMenu()
    {
        MainMenu.Instance.OpenMainMenu();
    }
}
