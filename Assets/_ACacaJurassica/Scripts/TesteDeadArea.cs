﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesteDeadArea : MonoBehaviour {

    public Transform Children;
    public float Offset=5;

    private Vector3 _position;
    private bool _checkDeadArea;

	void Start ()
    {
        _position = transform.position;
    }

    void Update ()
    {
        if (!_checkDeadArea)
            return;

        if(PassedOffset())
        {
            _checkDeadArea = false;

            Children.SetParent(transform);
            Children.localPosition = Vector3.zero;
        }
    }

    public void OnFoundMovement()
    {
        _position = transform.position;
        _checkDeadArea = true;

        Children.SetParent(null);
    }

    public void OnLostMovement()
    {
        _checkDeadArea = false;

        Children.SetParent(transform);
        Children.localPosition = Vector3.zero;
    }

    private bool PassedOffset()
    {
        if (transform.position.x > transform.position.x + Offset || transform.position.x < transform.position.x - Offset)
            return true;

        if (transform.position.y > transform.position.y + Offset || transform.position.y < transform.position.y - Offset)
            return true;

        if (transform.position.z > transform.position.z + Offset || transform.position.z < transform.position.z - Offset)
            return true;

        return false;
    }
}
