﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardTile : MonoBehaviour {

    public GameEnums.BoardTileId Id;
    public int LetterIndex;
    public int NumberIndex;
    public MeshRenderer PathMarker;
    public MeshRenderer DinoHeadIcon;
    public Transform DirectionArrow;

    public Transform Transform { get { return transform; } }
    public Vector3 Position { get { return transform.position; } }
    public bool MarkedForTrap { get; set; }

    private PotaTween _pathMarkerAnimation;
    private PotaTween _headIconAnimation;
    private PotaTween _directionArrowAnimation;

    void Start ()
    {
        _pathMarkerAnimation = PathMarker.GetComponent<PotaTween>();
        _headIconAnimation = DinoHeadIcon.GetComponent<PotaTween>();
        _directionArrowAnimation = DirectionArrow.GetComponent<PotaTween>();
    }
	
	void Update ()
    {
		
	}

    public void MarkForTrap()
    {
        MarkedForTrap = true;
    }

    public void UnmarkForTrap()
    {
        MarkedForTrap = false;
    }

    public void SetIconToDino(float delay, float duration, Material headIcon)
    {
        DinoHeadIcon.gameObject.SetActive(true);
        DinoHeadIcon.material = headIcon;
        Invoke("DisableIcon", duration);

        _headIconAnimation.SetDelay(delay).Play();
    }

    public void SetMarkerToDino(float delay, float duration, Material markerColor)
    {
        PathMarker.gameObject.SetActive(true);
        PathMarker.material = markerColor;

        Invoke("DisableMarker", duration + delay);

        _pathMarkerAnimation.SetDelay(delay).Play();
    }

    public void SetDirectionArrow(float delay, GameEnums.BoardDirections direction)
    {
        DirectionArrow.gameObject.SetActive(true);
        DirectionArrow.localEulerAngles = new Vector3(0, 0, GetArrowRotation(direction));

        _directionArrowAnimation.SetDelay(delay).Play();
    }

    private void DisableIcon()
    {
        _headIconAnimation.SetDelay(0).Reverse(() => DinoHeadIcon.gameObject.SetActive(false));
    }

    private void DisableMarker()
    {
        _pathMarkerAnimation.SetDelay(0).Reverse(() => PathMarker.gameObject.SetActive(false));

        if (DirectionArrow.gameObject.activeSelf)
            _directionArrowAnimation.SetDelay(0).Reverse(() => DirectionArrow.gameObject.SetActive(false));
    }

    private float GetArrowRotation(GameEnums.BoardDirections direction)
    {
        switch (direction)
        {
            case GameEnums.BoardDirections.Down:
                return -180;
                break;
            case GameEnums.BoardDirections.DownLeft:
                return -225;
                break;
            case GameEnums.BoardDirections.Left:
                return -270;
                break;
            case GameEnums.BoardDirections.UpLeft:
                return -315;
                break;
            case GameEnums.BoardDirections.Up:
                return 0;
                break;
            case GameEnums.BoardDirections.UpRight:
                return -45;
                break;
            case GameEnums.BoardDirections.Right:
                return -90;
                break;
            case GameEnums.BoardDirections.DownRight:
                return -135;
                break;
        }

        return 0;
    }
}
