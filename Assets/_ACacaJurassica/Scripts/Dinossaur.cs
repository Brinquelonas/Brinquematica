﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dinossaur : MonoBehaviour {

    [Serializable] public struct DinoVariables
    {
        public int NumberOfPathNodes;
        public float TimeShowingDirection;
        public float TimeToJumpOffBush;
        public float TimeToStartWalkingPath;
        public float TimeToWalkPathNodes;
        public float TimeToEatFood;
        public float HitsToCapture;
        public int PointsPerCapture;
    }

    [Header("Dinosaur Configuration")]
    public GameEnums.DinoColor Color;
    public GameEnums.DinoType Type;
    public GameEnums.TrapType[] Triggers;
    public int DinoIndex;
    public DinoVariables Variables;

    [Header("References")]
    public Material HeadIcon;
    public Material MarkerColor;
    public DinosaurAnimations Animations;

    public BoardPath Path { get; set; }
    public PotaTween PathWalk { get; set; }
    public PotaTween PathTurn { get; set; }
    public BoardTile PathFinalTile { get; set; }
    public List<int> DinoRotations { get; set; }

    public Action OnStartEating { get; set; }
    public Action OnFinishedEating { get; set; }
    public GameEnums.DinoState State { get; set; }

    private int _hitsTaken;
    private float _timeToRun;

    void Start ()
    {
        
	}
	
	void Update ()
    {
        /*if(State == GameEnums.DinoState.Walking)
        {
            transform.LookAt(Path.CurrentPosition);
            transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y, 0);
        }*/
    }

    #region SinglePlayer
    void OnMouseDown()
    {
        if (GameController.Instance.Type != GameEnums.GameType.Beginner)
            return;
        
        Hit();
    }

    public void Appear(float timeToRun = 0)
    {
        Animations.PlayAnimation(DinosaurAnimations.Anims.Appear);
        _timeToRun = timeToRun;

        Vector3 scale = transform.localScale;
        PotaTween.Create(gameObject, 2).
            SetScale(Vector3.zero, scale).
            SetDuration(0.5f).
            Play(() =>
            {

            });

        Invoke("IdleBush", 1);
    }

    public void IdleBush()
    {
        Animations.InBush = true;
        Animations.PlayAnimation(DinosaurAnimations.Anims.Idle);

        Invoke("Escape", _timeToRun);
    }

    public void Hit()
    {
        Animations.PlayAnimation(DinosaurAnimations.Anims.Hit);
        ParticlePlayer.Instance.PlayParticle(Particles.Hit, transform);

        _hitsTaken++;

        if (_hitsTaken >= Variables.HitsToCapture)
        {
            GameController.Instance.Points += Variables.PointsPerCapture;
            GameCanvas.Instance.UpdatePointsText(GameController.Instance.Points);

            GetComponent<Collider>().enabled = false;

            PotaTween.Create(gameObject, 2).
            SetScale(transform.localScale, Vector3.zero).
            SetDelay(0.25f).
            SetDuration(0.5f).
            Play(() =>
            {
                Destroy(gameObject);
            });
        }
    }

    public void Escape()
    {
        GetComponent<Collider>().enabled = false;

        CallOnFinishedEating();

        Animations.PlayAnimation(DinosaurAnimations.Anims.Appear);

        PotaTween.Create(gameObject, 2).
            SetScale(transform.localScale, Vector3.zero).
            SetDelay(0.5f).
            SetDuration(0.5f).
            Play(() =>
            {
                Destroy(gameObject, 1);
            });
    }
    #endregion

    public void StartPathWalkDelay()
    {
        transform.localEulerAngles = new Vector3(0, Path.CurrentZRotation, 0);

        Invoke("StartPathWalk", Variables.TimeToStartWalkingPath);

        State = GameEnums.DinoState.OnBush;
    }

    public void StartPathWalk()
    {
        SfxController.Instance.Play(SfxController.Instance.SfxDinoVoice1);

        transform.localEulerAngles = new Vector3(0, Path.CurrentZRotation, 0);
        //PathTurn = PotaTween.Create(gameObject).SetRotation(transform.localEulerAngles, new Vector3(0, Path.CurrentZRotation, 0), true).SetDuration(0.15f);
        //PathTurn.Play();

        PathWalk = PotaTween.Create(gameObject, 1).SetPosition(transform.localPosition, Path.CurrentLocalPosition, true).SetDuration(Variables.TimeToJumpOffBush);
        PathWalk.Play(CallbackPathWalk);

        Animations.PlayAnimation(DinosaurAnimations.Anims.Appear);
        State = GameEnums.DinoState.Walking;
    }

    public void ContinuePathWalk()
    {
        transform.localEulerAngles = new Vector3(0, Path.CurrentZRotation, 0);
        //PathTurn.SetRotation(transform.localEulerAngles, new Vector3(0, Path.CurrentZRotation, 0), true).Play();
        PathWalk.SetPosition(transform.localPosition, Path.CurrentLocalPosition, true).SetDuration(Variables.TimeToWalkPathNodes).Play(CallbackPathWalk);

        Animations.PlayAnimation(DinosaurAnimations.Anims.Running);
    }

    public bool TriggerTrap(GameEnums.TrapType trap)
    {
        for (int i = 0; i < Triggers.Length; i++)
        {
            if (Triggers[i] == trap || Triggers[i] == GameEnums.TrapType.All)
                return true;
        }

        return false;
    }

    private void CallbackPathWalk()
    {
        Path.Index++;

        if (Path.Index >= Variables.NumberOfPathNodes)
        {
            FinishedPathWalk();
            return;
        }

        Invoke("ContinuePathWalk", 0.01f);
    }

    private void FinishedPathWalk()
    {
        State = GameEnums.DinoState.Escaped;

        PathFinalTile.UnmarkForTrap();
        Destroy(gameObject);
    }

    public void SetBoardPath(List<BoardTile> boardTiles, Bushes bush)
    {
        Path = new BoardPath();
        Path.Initialize();

        Path.ZRotations.Add(bush.DinossaurRotation);

        for (int i = 0; i < boardTiles.Count; i++)
        {
            Path.AddTransformToPath(boardTiles[i].Transform);
            Path.ZRotations.Add(-Mathf.Abs(boardTiles[i].DirectionArrow.localEulerAngles.z));

            if (i + 1 == boardTiles.Count)
            {
                Path.FinalTileId = boardTiles[i].Id;
            }
        }
    }

    public void SetBoardPathOnTutorial(List<BoardTile> boardTiles, Bushes bush)
    {
        Path = new BoardPath();
        Path.Initialize();

        Path.ZRotations.Add(bush.DinossaurRotation);

        for (int i = 0; i < boardTiles.Count; i++)
        {
            Path.AddTransformToPath(boardTiles[i].Transform);
            Path.ZRotations.Add(bush.DinossaurRotation);

            if (i + 1 == boardTiles.Count)
            {
                Path.FinalTileId = boardTiles[i].Id;
            }
        }
    }

    public void StopPathAndStartEat(Vector3 foodPosition)
    {
        transform.LookAt(Path.CurrentPosition);
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, 0);

        PathWalk.Stop();
        PathWalk.SetPosition(transform.localPosition, foodPosition, true).SetDuration(Variables.TimeToWalkPathNodes).Play(() =>
        {
            Animations.PlayAnimation(DinosaurAnimations.Anims.Eating);
            State = GameEnums.DinoState.Eating;
            OnStartEating();

            Invoke("CallOnFinishedEating", Variables.TimeToEatFood);
        });
    }

    public void StartEating(bool timedCallback = true)
    {
        PathWalk.Pause();

        Animations.PlayAnimation(DinosaurAnimations.Anims.Eating);
        State = GameEnums.DinoState.Eating;

        if (OnStartEating != null)
            OnStartEating();

        if(timedCallback)
            Invoke("CallOnFinishedEating", Variables.TimeToEatFood);
    }

    public void StopEating()
    {
        Animations.PlayAnimation(DinosaurAnimations.Anims.Idle);
    }

    private void CallOnFinishedEating()
    {
        State = GameEnums.DinoState.Escaped;

        if (OnFinishedEating != null)
            OnFinishedEating();
    }

    public void StopDinossaur()
    {
        CancelInvoke();

        if(PathWalk != null)
            PathWalk.Stop();
    }
}
