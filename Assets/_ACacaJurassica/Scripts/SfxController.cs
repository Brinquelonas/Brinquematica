﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxController : MonoBehaviour {

    public static SfxController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<SfxController>();

            return _instance;
        }
    }
    private static SfxController _instance;

    public static bool SfxEnabled;

    public AudioSource Source;
    public AudioClip SfxRockClick;
    public AudioClip SfxEggClick;
    public AudioClip SfxCheckboxClick;
    public AudioClip SfxDinoVoice1;
    public AudioClip SfxDinoVoice2;
    public AudioClip SfxDinoRunning;
    public AudioClip SfxStar;
    public AudioClip Sparkle;
    public AudioClip Drums;

    void Start()
    {
        if(_instance == null)
            DontDestroyOnLoad(gameObject);
    }

    void Update()
    {

    }

    public void Play(AudioClip clip)
    {
        if(SfxEnabled)
            Source.PlayOneShot(clip);
    }

    public void Stop()
    {
        if (SfxEnabled)
            Source.Stop();
    }
}
