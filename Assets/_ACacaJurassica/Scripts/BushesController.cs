﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BushesController : MonoBehaviour {

    public Bushes[] Bushes;

    private List<int> _bushesIndexes = new List<int>();

	void Start ()
    {
        for (int i = 0; i < Bushes.Length; i++)
        {
            _bushesIndexes.Add(i);
        }
	}
	
	void Update ()
    {
		
	}

    public void ShakeBush(Bushes bush)
    {
        bush.GetComponentInChildren<Animator>().SetTrigger("Shake");
    }

    public Bushes GetRandomBush()
    {
        if(_bushesIndexes.Count == 0)
        {
            for (int i = 0; i < Bushes.Length; i++)
                _bushesIndexes.Add(i);
        }

        int index = _bushesIndexes[Random.Range(0, _bushesIndexes.Count)];
        _bushesIndexes.Remove(index);

        return Bushes[index];
    }
}
