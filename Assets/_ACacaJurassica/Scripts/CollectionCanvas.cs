﻿using EasyAR;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CollectionCanvas : MonoBehaviour {

    public static CollectionCanvas Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<CollectionCanvas>();

            return _instance;
        }
    }
    private static CollectionCanvas _instance;

    [Header("Configuration")]
    public CollectionEgg EggPrefab;
    public Transform EggContainer;
    public ImageTargetBehaviour BoardTracker;

    [Header("Animations")]
    public PotaTween Fader;
    public PotaTween Margin;
    public PotaTween ContainerFade;
    public PotaTween CallText;
    public PotaTween EggContainerTranslate;
    public Material DinosaurLockedBlack;

    [Header("Buttons")]
    public Button UpArrow;
    public Button DownArrow;
    public Button CallButton;
    public InputField InputButton;
    public Button Back;

    public AudioSource SourceRunning;
    public AudioSource SourceVoice;

    public List<CollectionEgg> Eggs { get; set; }
    public GameObject DinosaurModel { get; set; }
    public GameObject DinosaurBoardModel { get; set; }
    public CollectionEgg SelectedEgg { get; set; }
    public PotaTween DinosaurModelAnimation { get; set; }
    public bool OnCallButton { get; set; }

    public CollectionDinosaur CurrentDino { get { return SelectedEgg.Dinosaur; } }

    private List<List<CollectionEgg>> _eggsByRow;
    private PotaTween _dinoBoardAnimation;
    private int _numberOfRows = 0;
    private int _currentRow = 0;

    void Start()
    {
        AudioController.Instance.Play();

        InitializeEggs();

        LoadButtons();

        Margin.Play(() =>
        {
            Fader.Play(() =>
            {
                Back.interactable = true;
            });
        });

        BoardTracker.OnTargetFound.AddListener(() =>
        {
            DinosaurBoardModel.SetActive(true);

            if (CallText.gameObject.activeSelf)
            {
                CallText.Stop();
                CallText.Reverse(() => CallText.gameObject.SetActive(false));
            }

            DinosaurBoardModel.GetComponentInChildren<DinosaurModelAnimation>().JumpOffEgg();
        });

        BoardTracker.OnTargetLost.AddListener(() =>
        {
            DinosaurBoardModel.SetActive(false);

            if (!CallText.gameObject.activeSelf)
            {
                CallText.gameObject.SetActive(true);

                CallText.Stop();
                CallText.Play();
            }
        });
    }

	
	void Update () {
		
	}

    private void InitializeEggs()
    {
        Eggs = new List<CollectionEgg>();
        _eggsByRow = new List<List<CollectionEgg>>();
        _eggsByRow.Add(new List<CollectionEgg>());

        float[] xPositions = new float[] { 105f, 432f, 760f };
        float yPosition = 265;
        int xIndex = 0;
        int yIndex = 0;
        int rowIndex = 0;
        int numberOfEggs = CollectionController.Instance.Eggs.Length;

        for (int i = 0; i < numberOfEggs; i++)
        {
            if (i > 0 && i % 3 == 0)
            {
                xIndex = 0;
                yIndex++;
                rowIndex++;

                _eggsByRow.Add(new List<CollectionEgg>());
            }

            Eggs.Add(Instantiate(EggPrefab, EggContainer));
            Eggs[i].transform.localPosition = new Vector3(xPositions[xIndex], yPosition - (yIndex * yPosition), 0);

            _eggsByRow[rowIndex].Add(Eggs[i]);

            if (rowIndex >= 3)
            {
                for (int j = 0; j < _eggsByRow[rowIndex].Count; j++)
                {
                    _eggsByRow[rowIndex][j].EggAppear.InitialState();
                }
            }

            xIndex++;
        }

        for (int i = 0; i < Eggs.Count; i++)
        {
            Eggs[i].Initialize(CollectionController.Instance.Eggs[i], CollectionController.Dinosaurs[i]);

            if (CollectionController.Dinosaurs[i].Captured)
            {
                Eggs[i].SetEggCaptured();
            }
            else
            {
                Eggs[i].SetEggNormal(CollectionController.Dinosaurs[i].NumberOfCaptures, CollectionController.Dinosaurs[i].NumberOfCapturesToUnlock);
            }
        }

        _numberOfRows = yIndex + 1;
    }

    private void LoadButtons()
    {
        UpArrow.onClick.AddListener(() =>
        {
            _currentRow--;

            SfxController.Instance.Play(SfxController.Instance.SfxCheckboxClick);

            ReverseEggsAppear(_currentRow + 3);
            PlayEggsAppear(_currentRow);

            UpArrow.interactable = false;
            DownArrow.interactable = false;

            Vector3 from = new Vector3(0, EggContainer.localPosition.y, 0);
            Vector3 to = new Vector3(0, (_currentRow * 265) + 265, 0);

            EggContainerTranslate.SetPosition(from, to, true).Play(() =>
            {
                UpArrow.interactable = _currentRow > 0;
                DownArrow.interactable = true;
            });
        });

        DownArrow.onClick.AddListener(() =>
        {
            _currentRow++;

            SfxController.Instance.Play(SfxController.Instance.SfxCheckboxClick);

            ReverseEggsAppear(_currentRow - 1);
            PlayEggsAppear(_currentRow + 2);

            UpArrow.interactable = false;
            DownArrow.interactable = false;

            Vector3 from = new Vector3(0, EggContainer.localPosition.y, 0);
            Vector3 to = new Vector3(0, (_currentRow * 265) + 265, 0);

            EggContainerTranslate.SetPosition(from, to, true).Play(() =>
            {
                UpArrow.interactable = true;
                DownArrow.interactable = (_currentRow + 3) < _numberOfRows;
            });
        });

        CallButton.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

            LoadDinoOnBoard();

            OnCallButton = true;

            DinosaurModel.GetComponent<PotaTween>().SetEaseEquation(Ease.Equation.InBack).SetDelay(0).Reverse();

            ContainerFade.Play(() =>
            {
                CallText.gameObject.SetActive(true);
                ContainerFade.gameObject.SetActive(false);

                CallText.Play();
            });
        });

        Back.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);

            //OptionsController.Instance.LockButton();

            if (OnCallButton)
            {
                if (DinosaurBoardModel != null)
                    Destroy(DinosaurBoardModel);

                if(CallText.gameObject.activeSelf)
                    CallText.Reverse(() => CallText.gameObject.SetActive(false));

                ContainerFade.gameObject.SetActive(true);
                ContainerFade.Reverse();

                OnCallButton = false;
            }
            else
            {
                Margin.Reverse();
                Fader.SetDelay(0).Reverse();
                Invoke("BackToMenu", 1);
            }
        });

        UpArrow.interactable = false;
    }

    private void LockButtons()
    {
        UpArrow.interactable = false;
        DownArrow.interactable = false;

        for (int i = 0; i < Eggs.Count; i++)
        {
            Eggs[i].Button.interactable = false;
        }
    }

    private void UnlockButtons()
    {
        UpArrow.interactable = true;
        DownArrow.interactable = true;

        for (int i = 0; i < Eggs.Count; i++)
        {
            Eggs[i].Button.interactable = true;
        }
    }

    private void PlayEggsAppear(int eggsRow)
    {
        for (int i = 0; i < _eggsByRow[eggsRow].Count; i++)
        {
            _eggsByRow[eggsRow][i].EggAppear.SetEaseEquation(Ease.Equation.OutBack).SetDelay(0.3f).Play();
        }
    }

    private void ReverseEggsAppear(int eggsRow)
    {
        for (int i = 0; i < _eggsByRow[eggsRow].Count; i++)
        {
            _eggsByRow[eggsRow][i].EggAppear.SetEaseEquation(Ease.Equation.InBack).SetDelay(0f).Reverse();
        }
    }

    public void ClickOnEgg(CollectionEgg egg)
    {
        LockButtons();

        SfxController.Instance.Play(SfxController.Instance.SfxEggClick);

        if (DinosaurModel == null)
        {
            SelectedEgg = egg;
            DinosaurModel = Instantiate(egg.DinosaurModel);
            CallButton.interactable = SelectedEgg.DinoCaptured;
            InputButton.interactable = SelectedEgg.DinoCaptured;

            SelectedEgg.PlayGlow();
            DinosaurModel.GetComponent<PotaTween>().SetEaseEquation(Ease.Equation.OutBack).SetDelay(0.25f).Play();
            DinosaurModel.GetComponentInChildren<DinosaurAnimations>().PlayAnimation(DinosaurAnimations.Anims.Appear);
            Invoke("PlayDinoIdle", 1.025f);

            InputName.Instance.SetDinoText(CurrentDino.Name);
        }
        else
        {
            GameObject dinoOldModel = DinosaurModel;

            SelectedEgg.ReverseGlow();
            dinoOldModel.GetComponent<PotaTween>().SetEaseEquation(Ease.Equation.InBack).SetDelay(0).Reverse(() =>
            {
                SelectedEgg = egg;
                DinosaurModel = Instantiate(egg.DinosaurModel);
                CallButton.interactable = SelectedEgg.DinoCaptured;
                InputButton.interactable = SelectedEgg.DinoCaptured;

                SelectedEgg.PlayGlow();
                DinosaurModel.GetComponent<PotaTween>().SetEaseEquation(Ease.Equation.OutBack).SetDelay(0.25f).Play();
                //DinosaurModel.GetComponentInChildren<DinosaurAnimations>().PlayAnimation(DinosaurAnimations.Anims.Appear);
                Invoke("PlayDinoIdle", 1.025f);

                InputName.Instance.SetDinoText(CurrentDino.Name);

                Destroy(dinoOldModel);
            });
        }

        Invoke("UnlockButtons", 1.25f);
    }

    private void PlayDinoIdle()
    {
        DinosaurModel.GetComponentInChildren<DinosaurAnimations>().PlayAnimation(DinosaurAnimations.Anims.Idle);
    }

    private void PlayBoardDinoIdle()
    {
        //DinosaurBoardModel.GetComponentInChildren<DinosaurAnimations>().PlayAnimation(DinosaurAnimations.Anims.Idle);
    }

    private void LoadDinoOnBoard()
    {
        DinosaurBoardModel = Instantiate(DinosaurModel);

        Transform dino = DinosaurBoardModel.transform;
        dino.parent = BoardTracker.transform;
        dino.localPosition = Vector3.zero;
        dino.localEulerAngles = new Vector3(0, 150, 0);
        dino.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        DinosaurBoardModel.GetComponentInChildren<DinosaurModelAnimation>().SourceVoice = SourceVoice;
        DinosaurBoardModel.GetComponentInChildren<DinosaurModelAnimation>().SourceRunning = SourceRunning;
    }

    void BackToMenu()
    {
        AudioController.Instance.Stop();
        SceneManager.LoadScene("MainMenu");
    }
}
