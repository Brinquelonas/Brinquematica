﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TrapDigitalDrag : MonoBehaviour
{
    public MeshRenderer Renderer;

    private float _minWorldValue = -0.322f;
    private float _maxWorldValue = 0.322f;

    private PotaTween _fadeAnimation;
    private PotaTween _highlightAnimation;
    private GameObject _renderObject;

    public bool Dragging { get; set; }
    public Action OnDragBegin { get; set; }
    public Action OnDragging { get; set; }
    public Action OnDragEnd { get; set; }
    public Action<BoardTile> OnChangeBoardTile { get; set; }

    void Start ()
    {
        _renderObject = Renderer.gameObject;
        _fadeAnimation = PotaTween.Create(_renderObject).SetAlpha(0f, 1f).SetScale(Vector3.zero, new Vector3(0.65f, 0.65f, 0.65f)).SetDuration(0.3f);
        _highlightAnimation = PotaTween.Create(_renderObject, 1).SetAlpha(1, 0.35f).SetScale(new Vector3(0.65f, 0.65f, 0.65f), new Vector3(0.45f, 0.45f, 0.45f)).SetDuration(0.75f).SetLoop(LoopType.PingPong);

        _renderObject.SetActive(false);
    }
	
	void Update ()
    {

    }

    private void FadeIn()
    {
        _renderObject.SetActive(true);
        _fadeAnimation.Play(() => Highlight());
    }

    private void FadeOut()
    {
        _highlightAnimation.Stop();
        _fadeAnimation.Reverse(() => _renderObject.SetActive(false));
    }

    private void Highlight()
    {
        _highlightAnimation.Play();
    }

    private void OnMouseDown()
    {
        FadeIn();
        Dragging = true;

        if(OnDragBegin != null)
            OnDragBegin();
    }

    private void OnMouseDrag()
    {
        float leftBoardPosition = BoardTilesController.Instance.LeftBoardScreenPosition;
        float rightBoardPosition = BoardTilesController.Instance.RightBoardScreenPosition;
        float upBoardPosition = BoardTilesController.Instance.UpBoardScreenPosition;
        float downBoardPosition = BoardTilesController.Instance.DownBoardScreenPosition;

        /*print("Left " + leftBoardPosition);
        print("Right " + rightBoardPosition);
        print("Up " + upBoardPosition);
        print("Down " + downBoardPosition);
        print("Mouse " + Input.mousePosition);*/

        if (Input.mousePosition.x < leftBoardPosition || Input.mousePosition.x > rightBoardPosition)
            return;
        if (Input.mousePosition.y < downBoardPosition || Input.mousePosition.y > upBoardPosition)
            return;

        float xDivisor = rightBoardPosition - leftBoardPosition;
        float yDivisor = upBoardPosition - downBoardPosition;
        /*print("XDivisor " + xDivisor);
        print("YDivisor " + yDivisor);*/

        double xScreenPosition = (Input.mousePosition.x / xDivisor) - ((rightBoardPosition / xDivisor) - 1);
        double yScreenPosition = (Input.mousePosition.y / yDivisor) - ((upBoardPosition / yDivisor) - 1);
        /*print("XScreen " + xScreenPosition);
        print("XScreen " + yScreenPosition);*/

        float xWorldPosition = Mathf.Lerp(_minWorldValue, _maxWorldValue, (float) xScreenPosition);
        float zWorldPosition = Mathf.Lerp(_minWorldValue, _maxWorldValue, (float) yScreenPosition);

        transform.localPosition = new Vector3(xWorldPosition, transform.localPosition.y, zWorldPosition);

        if (OnDragging != null)
            OnDragging();
    }

    private void OnMouseUp()
    {
        FadeOut();
        Dragging = false;

        if (OnDragEnd != null)
            OnDragEnd();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BoardTile"))
        {
            //Renderer.material.color = Color.green;

            if (OnChangeBoardTile != null)
                OnChangeBoardTile(other.GetComponent<BoardTile>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        /*if (other.CompareTag("BoardTile"))
        {
            //Renderer.material.color = Color.white;
        }*/
    }
}
