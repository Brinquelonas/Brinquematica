﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public static AudioController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<AudioController>();

            return _instance;
        }
    }
    private static AudioController _instance;

    public static bool MusicEnabled;

    public AudioSource Source;

	void Start ()
    {
		
	}
	
	void Update () {
		
	}

    public void Play()
    {
        if(MusicEnabled)
            Source.Play();
    }

    public void Stop()
    {
        Source.Stop();
    }
}
