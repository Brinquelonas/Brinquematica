﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameCanvas : MonoBehaviour {

    public static GameCanvas Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameCanvas>();

            return _instance;
        }
    }
    private static GameCanvas _instance;

    public PotaTween Margin;
    public PotaTween Fader;
    public PotaTween Timer;
    public PotaTween Points;
    public PotaTween TrapsButton;
    //public PotaTween TrapsText;
    public TweenAnimations Captured;

    public Text PointsText;
    public Button ExitButton;

    void Start ()
    {
        //TrapsText.gameObject.SetActive(false);

        TrapsButton.InitialState();
        Timer.InitialState();
        Points.InitialState();
        Captured.Animations[0].InitialState();

        ExitButton.onClick.AddListener(() =>
        {
            Fader.Reverse(() => SceneManager.LoadScene("MainMenu"));
        });

        Margin.Play();

        Fader.Play(() =>
        {
            TrapsButton.Play(() =>
            {
                //TrapsText.gameObject.SetActive(true);
                //TrapsText.Play();
            });

            Timer.Play();
            Points.Play();
        });
    }
	
	void Update ()
    {
		
	}

    public void UpdatePointsText(float points)
    {
        PointsText.text = points.ToString("0000");
    }
}
