﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameLevelVariables : MonoBehaviour {

    [Serializable]
    public struct DinosaurLevelVariables
    {
        public string DinosaurName;
        public Dinossaur DinosaurPrefab;
        public Dinossaur.DinoVariables DinosaurValues;
        public bool OverrideDinosaurDefaultValues;
    }

    [Header("Level Configuration")]
    public string LevelName;
    public int LevelId;

    [Header("Dinosaur Configuration")]
    public DinosaurLevelVariables[] Dinosaurs;
    public int[] DinosaursSpawnPercentage;

    [Header("Time Configuration")]
    public float LevelTotalTime;
    public float TimeToSpawnDinosaur;
    public float TimeToSpawnFirstDinosaur;

    [Header("Points Configuration")]
    public float Star1Points;
    public float Star2Points;
    public float Star3Points;

    [Header("Single Player")]
    public float TimeDecreasingRate;
    public float MinimumTimeToSpawn;
    public float MinimumTimeToEat;

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public Dinossaur DinosaurToSpawn()
    {
        int sortedIndex = 0;
        int index = Random.Range(0, 101);

        for (int i = 0; i < DinosaursSpawnPercentage.Length; i++)
        {
            if(index <= DinosaursSpawnPercentage[i])
            {
                sortedIndex = i;
                break;
            }
        }

        if (sortedIndex >= Dinosaurs.Length)
            sortedIndex = Dinosaurs.Length - 1;

        Dinossaur dino = Instantiate(Dinosaurs[sortedIndex].DinosaurPrefab);

        if (Dinosaurs[sortedIndex].OverrideDinosaurDefaultValues)
            dino.Variables = Dinosaurs[sortedIndex].DinosaurValues;

        return dino;
    }
}
