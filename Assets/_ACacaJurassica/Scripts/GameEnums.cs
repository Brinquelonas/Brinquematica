﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnums : MonoBehaviour
{
    public enum BoardDirections { Down, DownLeft, Left, UpLeft, Up, UpRight, Right, DownRight, None };

    public enum BoardTileId
    {
        A1 = 0,
        A2 = 1,
        A3 = 2,
        A4 = 3,
        B1 = 4,
        B2 = 5,
        B3 = 6,
        B4 = 7,
        C1 = 8,
        C2 = 9,
        C3 = 10,
        C4 = 11,
        D1 = 12,
        D2 = 13,
        D3 = 14,
        D4 = 15
    }

    public enum GameType { Beginner, Experient, Professional }

    public enum DinoType { Default, Spikes, Fins, Membrane, Rare }

    public enum DinoState { OnBush, Walking, Eating, Captured, Escaped }

    public enum DinoColor { Green, Blue, Orange, Black, Purple, Red, White, Gold }

    public enum TrapType { BoxMeat, BoxVeggie, BoxFruit, NetMeat, NetVeggie, NetFruit, RopeMeat, RopeVeggie, RopeFruit, All, None }

    public enum TrapState { OnTracker, OnBoard, ReadyToCapture, Broken }
}
