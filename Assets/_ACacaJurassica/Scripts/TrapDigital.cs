﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDigital : MonoBehaviour {

    public GameEnums.TrapType TrapType;
    public TrapAnimations Animations;
    public TrapCollider Collider;

    public Food Food;
    public TrapDigitalDrag Drag;

    public BoardTile SettedTile { get; set; }
    public Dinossaur CatchedDino { get; set; }

    void Start()
    {
        Collider.TriggerEnter += StartedCollision;
        Collider.TriggerExit += StoppedCollision;

        Drag.OnDragBegin += () =>
        {
            Drag.transform.SetParent(transform.parent);
        };

        Drag.OnDragEnd += () =>
        {
            Drag.transform.SetParent(transform);
            Drag.transform.localPosition = new Vector3(0, 0.1f, 0);
        };

        Drag.OnChangeBoardTile += (b) =>
        {
            transform.localPosition = new Vector3(b.transform.localPosition.x, 0, b.transform.localPosition.z);
        };
    }

    void Update()
    {

    }

    private void StartedCollision(Collider other)
    {
        if (other.CompareTag("BoardTile"))
        {
            SettedTile = other.GetComponent<BoardTile>();
        }

        if (other.CompareTag("Dinossaur"))
        {
            Dinossaur dino = other.GetComponent<Dinossaur>();

            if (dino.TriggerTrap(TrapType))
            {
                if (SettedTile.Id == dino.Path.FinalTileId)
                {
                    CatchedDino = dino;
                    CatchedDino.StartEating();
                    Food.FoodDisappearing.SetDuration(CatchedDino.Variables.TimeToEatFood).Play();

                    Invoke("CatchDino", 2.5f);
                }
                else
                {
                    Invoke("DestroyTrap", 1f);
                    Animations.BreakTrap();
                }
            }
            else
            {
                Invoke("DestroyTrap", 1f);
                Animations.BreakTrap();
            }
        }
    }

    private void StoppedCollision(Collider other)
    {
        
    }

    private void CatchDino()
    {
        TrapDigitalController.Instance.PlayCapturedText();
        TrapDigitalController.Instance.UpdatePoints(CatchedDino.Variables.PointsPerCapture);
        DinossaurController.Instance.MarkDinosaurForCollection(CatchedDino.DinoIndex);
        SfxController.Instance.Play(SfxController.Instance.SfxDinoVoice1);

        CatchedDino.StopEating();
        CatchedDino.transform.position = Animations.DinosaurPosition.position;
        CatchedDino.transform.SetParent(Animations.DinosaurBone);

        Food.transform.SetParent(Animations.DinosaurBone);
        Food.FoodDisappearing.Play();

        Animations.CaptureDino();

        Invoke("DestroyDino", 2f);
        Invoke("DestroyTrap", 2f);
    }

    private void DestroyDino()
    {
        Destroy(CatchedDino.gameObject);
    }

    private void DestroyTrap()
    {
        Destroy(gameObject);
    }
}
