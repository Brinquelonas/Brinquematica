﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour {

    public static OptionsController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<OptionsController>();

            return _instance;
        }
    }
    private static OptionsController _instance;

    private static bool _initialized;

    public Button OptionsButton;
    public Button OptionsFadeButton;
    public Button ExitButton;
    public Button LeaveMatchButton;
    public Toggle MusicToggle;
    public Toggle SfxToggle;
    public PotaTween OptionsPopup;
    public PotaTween OptionsFade;

    public GameObject Popup;
    public GameObject Fader;

    void Start ()
    {
        OptionsButton.onClick.AddListener(() =>
        {
            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);
            OptionsButton.interactable = false;

            Popup.SetActive(true);
            Fader.SetActive(true);

            OptionsPopup.Play();
            OptionsFade.Play(() =>
            {
                OptionsFadeButton.interactable = true;
            });
        });

        OptionsFadeButton.onClick.AddListener(() =>
        {
            OptionsFadeButton.interactable = false;
            OptionsFade.Reverse(() => Fader.SetActive(false));
            OptionsPopup.Reverse(() =>
            {
                OptionsButton.interactable = true;
                Popup.SetActive(false);
            });
        });

        ExitButton.onClick.AddListener(() =>
        {
            OptionsPopup.Reverse();
            OptionsFade.SetAlpha(0.5f, 1).Play(() =>
            {
                Application.Quit();
            });
        });
        
        if(!_initialized)
        {
            _initialized = true;

            SfxController.SfxEnabled = true;
            AudioController.MusicEnabled = true;
        }

        SfxToggle.isOn = SfxController.SfxEnabled;
        MusicToggle.isOn = AudioController.MusicEnabled;

        SfxToggle.onValueChanged.AddListener((value) =>
        {
            SfxController.SfxEnabled = value;
        });

        MusicToggle.onValueChanged.AddListener((value) =>
        {
            if(value == true)
            {
                AudioController.MusicEnabled = true;
                AudioController.Instance.Play();
            }
            else
            {
                AudioController.MusicEnabled = false;
                AudioController.Instance.Stop();
            }
        });
    }
	
	void Update ()
    {
		
	}

    public void LockButton()
    {
        OptionsButton.interactable = false;
    }

    public void UnlockButton()
    {
        OptionsButton.interactable = true;
    }
}
