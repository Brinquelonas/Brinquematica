﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Particles
{
    Hit
}

public class ParticlePlayer : MonoBehaviour {

    [System.Serializable]
    public struct ParticlePrefab
    {
        public ParticleSystem Particle;
        public Particles Enum;
    }

    private static ParticlePlayer _instance;
    public static ParticlePlayer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ParticlePlayer>();

            return _instance;
        }
    }

    public List<ParticlePrefab> Particles = new List<ParticlePrefab>();

    public ParticleSystem PlayParticle(Particles particle, Transform trans)
    {
        ParticleSystem part = Instantiate(FindParticle(particle), trans.position, trans.rotation);
        part.Play(true);

        return part;
    }

    private ParticleSystem FindParticle(Particles particle)
    {
        foreach (var p in Particles)
        {
            if (p.Enum == particle)
                return p.Particle;
        }

        return null;
    }
}
