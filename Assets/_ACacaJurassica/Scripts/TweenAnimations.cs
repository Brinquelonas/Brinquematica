﻿using System;
using UnityEngine;

public class TweenAnimations : MonoBehaviour {

    public PotaTween[] Animations;

	void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void Play(int index, Action callback = null)
    {
        Animations[index].Play(callback);
    }

    public void Reverse(int index, Action callback = null)
    {
        Animations[index].Reverse(callback);
    }

    public void Play(string tag, Action callback = null)
    {
        for (int i = 0; i < Animations.Length; i++)
        {
            if(Animations[i].Tag == tag)
                Animations[i].Play(callback);
        }
    }

    public void Reverse(string tag, Action callback = null)
    {
        for (int i = 0; i < Animations.Length; i++)
        {
            if (Animations[i].Tag == tag)
                Animations[i].Reverse(callback);
        }
    }

    public void PlayAll(Action callback = null)
    {
        float duration = 0;
        int callbackIndex = 0;

        for (int i = 0; i < Animations.Length; i++)
        {
            if (Animations[i].Duration > duration)
            {
                duration = Animations[i].Duration;
                callbackIndex = i;
            }
        }

        for (int i = 0; i < Animations.Length; i++)
        {
            Animations[i].Play(callbackIndex == i? callback : null);
        }
    }

    public void ReverseAll(Action callback = null)
    {
        float duration = 0;
        int callbackIndex = 0;

        for (int i = 0; i < Animations.Length; i++)
        {
            if (Animations[i].Duration > duration)
            {
                duration = Animations[i].Duration;
                callbackIndex = i;
            }
        }

        for (int i = 0; i < Animations.Length; i++)
        {
            Animations[i].Reverse(callbackIndex == i ? callback : null);
        }
    }
}
