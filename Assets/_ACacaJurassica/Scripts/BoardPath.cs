﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardPath : MonoBehaviour {

    public List<Transform> Transforms;
    public List<Vector3> Positions;
    public List<float> ZRotations;

    public int Index { get; set; }

    public Transform CurrentTransform { get { return Transforms[Index]; } }
    public Vector3 CurrentPosition { get { return Positions[Index]; } }
    public Vector3 CurrentLocalPosition { get { return Transforms[Index].localPosition; } }
    public float CurrentZRotation { get { return ZRotations[Index]; } }

    public GameEnums.BoardTileId FinalTileId;

    public void Initialize()
    {
        Transforms = new List<Transform>();
        Positions = new List<Vector3>();
        ZRotations = new List<float>();
        Index = 0;
    }

    public void AddTransformToPath(Transform transform)
    {
        Transforms.Add(transform);
        Positions.Add(transform.position);
    }

    public void ClearPath()
    {
        Transforms = new List<Transform>();
        Positions = new List<Vector3>();
        ZRotations = new List<float>();
    }
}
