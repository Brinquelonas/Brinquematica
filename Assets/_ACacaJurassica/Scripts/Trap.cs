﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyAR;


public class Trap : MonoBehaviour {

    public GameEnums.TrapType TrapType;
    public TrapAnimations Animations;
    public TrapCollider TrapBase;
    public Food TrapFood;
    public ImageTargetBehaviour Tracker;

    public bool SettedOnBoard { get; set; }
    public BoardTile SettedTile { get; set; }
    public Dinossaur CatchedDino { get; set; }

    public GameEnums.TrapState State { get; set; }

    void Start ()
    {
        Tracker.OnTargetFound.AddListener(() => TargetFound());
        Tracker.OnTargetLost.AddListener(() => TargetLost());

        TrapBase.TriggerEnter += StartedCollision;
        TrapBase.TriggerExit += StoppedCollision;
    }

    void Update ()
    {
		
	}

    private void TargetFound()
    {
        Animations.BuildTrap();
    }

    private void TargetLost()
    {
        if(State == GameEnums.TrapState.OnTracker)
        {

        }

        if (State == GameEnums.TrapState.OnBoard)
        {
            Animations.BreakTrap();
        }

        if (State == GameEnums.TrapState.ReadyToCapture)
        {
            Animations.CaptureDino();

            if (CatchedDino != null)
                CatchDino();
        }

        SettedOnBoard = false;
        SettedTile = null;
        State = GameEnums.TrapState.OnTracker;
    }

    private void StartedCollision(Collider other)
    {
        if(other.CompareTag("BoardTile"))
        {
            SettedOnBoard = true;
            SettedTile = other.GetComponent<BoardTile>();
            State = GameEnums.TrapState.OnBoard;
        }

        if(other.CompareTag("Dinossaur"))
        {
            Dinossaur dino = other.GetComponent<Dinossaur>();

            if (dino.TriggerTrap(TrapType))
            {
                if (SettedOnBoard && SettedTile.Id == dino.Path.FinalTileId)
                {
                    State = GameEnums.TrapState.ReadyToCapture;
                    transform.parent = SettedTile.transform;

                    CatchedDino = dino;
                    CatchedDino.StartEating();
                    TrapFood.FoodDisappearing.SetDuration(CatchedDino.Variables.TimeToEatFood).Play();

                    CatchedDino.OnFinishedEating += () =>
                    {
                        Animations.BreakTrap();
                        State = GameEnums.TrapState.Broken;

                        if (CatchedDino != null)
                            Destroy(CatchedDino.gameObject);
                    };
                }
                else
                {
                    Animations.BreakTrap();
                    State = GameEnums.TrapState.Broken;
                }
            }
            else
            {
                Animations.BreakTrap();
                State = GameEnums.TrapState.Broken;
            }
        }
    }

    private void StoppedCollision(Collider other)
    {
        
    }

    private void CatchDino()
    {
        PlayCapturedText();
        UpdatePoints(CatchedDino.Variables.PointsPerCapture);
        DinossaurController.Instance.MarkDinosaurForCollection(CatchedDino.DinoIndex);
        SfxController.Instance.Play(SfxController.Instance.SfxDinoVoice1);

        CatchedDino.StopEating();
        CatchedDino.transform.position = Animations.DinosaurPosition.position;
        CatchedDino.transform.SetParent(Animations.DinosaurBone);

        TrapFood.transform.SetParent(Animations.DinosaurBone);
        TrapFood.FoodDisappearing.Play();

        Animations.CaptureDino();

        Invoke("DestroyDino", 2f);
        Invoke("DestroyTrap", 2f);
    }

    private void DestroyDino()
    {
        Destroy(CatchedDino.gameObject);
    }

    private void DestroyTrap()
    {
        Destroy(gameObject);
    }

    public void UpdatePoints(float points)
    {
        GameController.Instance.Points += points;
        GameCanvas.Instance.UpdatePointsText(GameController.Instance.Points);
        DinossaurController.Instance.MarkDinosaurForCollection(CatchedDino.DinoIndex);
    }

    public void PlayCapturedText()
    {
        GameCanvas.Instance.Captured.Play(0);
        Invoke("ReverseCapturedText", 1.5f);
    }

    private void ReverseCapturedText()
    {
        GameCanvas.Instance.Captured.Play(1);
    }
}
