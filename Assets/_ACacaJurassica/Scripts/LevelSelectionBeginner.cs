﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectionBeginner : MonoBehaviour
{

    public static LevelSelectionBeginner Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<LevelSelectionBeginner>();

            return _instance;
        }
    }
    private static LevelSelectionBeginner _instance;

    public static LevelSelection.LevelItem[] LevelItems;
    public GameLevelVariables[] LevelVariables;

    public static GameLevelVariables CurrentLevelVariables;
    public static LevelSelection.LevelItem CurrentLevelItem;
    public static int CurrentLevelIndex;
    public static int NextLevelIndex;

    void Start()
    {
        if (LevelItems == null)
        {
            LevelItems = new LevelSelection.LevelItem[LevelVariables.Length];

            for (int i = 0; i < LevelItems.Length; i++)
            {
                LevelItems[i].Index = i;
                LevelItems[i].Unlocked = false;
                LevelItems[i].Played = false;
                LevelItems[i].Points = 0;
                LevelItems[i].StarsUnlocked = new bool[] { false, false, false };
            }

            LevelItems[0].Unlocked = true;
        }
    }

    void Update()
    {

    }

    public void SetCurrentLevel(int index)
    {
        //CurrentLevelItem = LevelItems[index];
        CurrentLevelVariables = LevelVariables[index];
        CurrentLevelIndex = index;

        if (index + 1 < LevelItems.Length)
            NextLevelIndex = index + 1;
    }
}
