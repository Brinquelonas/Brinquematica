﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathWindowLayout : MonoBehaviour {

    public GameObject[] DirectionArrows;

    private float _leftRotation = 0;
    private float _leftUpRotation = 45;
    private float _upRotation = 90;
    private float _rightRotation = 180;
    private float _rightUpRotation = 135;
    private float _rightDownRotation = 225;
    private float _downRotation = 270;
    private float _leftDownRotation = 315;
    

    void Start ()
    {
		
	}
	
	void Update ()
    {
		
	}

    public void SetDirectionsToArrows(List<GameEnums.BoardDirections> directions)
    {
        for (int i = 0; i < DirectionArrows.Length; i++)
        {
            SetArrowRotation(DirectionArrows[i].transform, directions[i]);
        }
    }

    private void SetArrowRotation(Transform arrowTransform, GameEnums.BoardDirections direction)
    {
        switch (direction)
        {
            case GameEnums.BoardDirections.Down : RotateArrow(arrowTransform, _downRotation);
                break;
            case GameEnums.BoardDirections.DownLeft : RotateArrow(arrowTransform, _leftDownRotation);
                break;
            case GameEnums.BoardDirections.Left : RotateArrow(arrowTransform, _leftRotation);
                break;
            case GameEnums.BoardDirections.UpLeft : RotateArrow(arrowTransform, _leftUpRotation);
                break;
            case GameEnums.BoardDirections.Up : RotateArrow(arrowTransform, _upRotation);
                break;
            case GameEnums.BoardDirections.UpRight : RotateArrow(arrowTransform, _rightUpRotation);
                break;
            case GameEnums.BoardDirections.Right : RotateArrow(arrowTransform, _rightRotation);
                break;
            case GameEnums.BoardDirections.DownRight : RotateArrow(arrowTransform, _downRotation);
                break;
        }
    }

    private void RotateArrow(Transform arrowTransform, float rotationModifier)
    {
        Vector3 rotation = arrowTransform.localEulerAngles;

        arrowTransform.localEulerAngles = new Vector3(rotation.x, rotation.y, rotationModifier);
    }
}
