﻿using System;
using UnityEngine;
using EasyAR;

public class TutorialTrapController : MonoBehaviour {

    public static TutorialTrapController Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<TutorialTrapController>();

            return _instance;
        }
    }
    private static TutorialTrapController _instance;

    public float Points { get; set; }
    public Action DinoCaptured { get; set; }

    public Transform TrapsContainer;
    public Transform TrapsTemplate;
    public Transform TrapTarget;
    public TutorialTrap Trap;

    void Start()
    {
        
    }

    void Update()
    {

    }

    public void CreateTrapOnBoard(TutorialTrap trap)
    {
        if (MainMenu.GameModeSelected != GameEnums.GameType.Experient)
            return;

        /*TutorialTrap newTrap = Instantiate(trap, TrapsContainer);
        newTrap.transform.localPosition = TrapsTemplate.localPosition;
        newTrap.transform.localEulerAngles = TrapsTemplate.localEulerAngles;
        newTrap.transform.localScale = TrapsTemplate.localScale;
        newTrap.TrapBase.Open();*/

        Trap.transform.SetParent(TrapsContainer);
        Trap.transform.localPosition = TrapsTemplate.localPosition;
        Trap.transform.localEulerAngles = TrapsTemplate.localEulerAngles;
        Trap.transform.localScale = TrapsTemplate.localScale;
        Trap.Animations.BuildTrap();
    }

    public void UpdatePoints(float points)
    {
        Points += points;
        GameCanvas.Instance.UpdatePointsText(Points);
    }

    public void PlayCapturedText()
    {
        GameCanvas.Instance.Captured.Play(0, () =>
        {
            if (DinoCaptured != null)
                DinoCaptured();
        });

        Invoke("ReverseCapturedText", 1.5f);
    }

    private void ReverseCapturedText()
    {
        GameCanvas.Instance.Captured.Play(1);
    }
}
