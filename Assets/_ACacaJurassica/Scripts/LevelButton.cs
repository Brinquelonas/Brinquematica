﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour {

    public GameObject Locked;
    public GameObject Unlocked;
    public Image UnlockedSprite;
    public GameObject[] Stars;
    public Button Button;
    public TweenAnimations Animations;

    public int Index { get; set; }
    public bool LevelUnlocked { get; set; }
    public bool[] StarsUnlocked { get; set; }
    public GameEnums.GameType GameMode { get; set; }

    void Start ()
    {
        
    }
	
	void Update ()
    {
		
	}

    public void Initialize(GameEnums.GameType gameMode, bool unlocked, bool[] starsUnlocked)
    {
        LevelUnlocked = unlocked;
        GameMode = gameMode;

        Stars[0].SetActive(starsUnlocked[0]);
        Stars[1].SetActive(starsUnlocked[1]);
        Stars[2].SetActive(starsUnlocked[2]);

        Unlocked.gameObject.SetActive(LevelUnlocked);
        Locked.gameObject.SetActive(!LevelUnlocked);

        Button.interactable = LevelUnlocked;
        Button.onClick.AddListener(() =>
        {
            if(Index == 0)
            {
                if(GameMode == GameEnums.GameType.Experient)
                {
                    LevelSelectionExperient.Instance.SetCurrentLevel(Index);
                }

                if (GameMode == GameEnums.GameType.Professional)
                {
                    LevelSelectionProfessional.Instance.SetCurrentLevel(Index);
                }

                MainMenu.Instance.ChangeSceneGameplayTutorial();
                SfxController.Instance.Play(SfxController.Instance.SfxRockClick);
                LevelSelectionCanvas.Instance.CloseLevelSelection();
                return;
            }

            if(GameMode == GameEnums.GameType.Experient)
            {
                MainMenu.Instance.ChangeSceneGameplayExperient();
                LevelSelectionExperient.Instance.SetCurrentLevel(Index);
            }

            if (GameMode == GameEnums.GameType.Professional)
            {
                MainMenu.Instance.ChangeSceneGameplayProfessional();
                LevelSelectionProfessional.Instance.SetCurrentLevel(Index);
            }

            SfxController.Instance.Play(SfxController.Instance.SfxRockClick);
            LevelSelectionCanvas.Instance.CloseLevelSelection();
        });
    }
}
