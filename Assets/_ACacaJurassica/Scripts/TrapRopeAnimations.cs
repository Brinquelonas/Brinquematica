﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapRopeAnimations : TrapAnimations {

    public override void Update()
    {
        base.Update();
    }

    public override void BuildTrap()
    {
        base.BuildTrap();

        PlayAnimation(Animations.Build);
        BuildAnimation.Play();
        BuildParticles.Play();
    }

    public override void BreakTrap()
    {
        base.BreakTrap();

        PlayAnimation(Animations.Break);
        BreakAnimation.SetDelay(1).Play();
        BreakParticles.Play();
    }

    public override void CaptureDino()
    {
        base.CaptureDino();

        //CaptureAnimation.SetDelay(1).Play();

        PlayAnimation(Animations.Capture);
        Invoke("CaptureParticlesDelay", 1);
    }

    private void CaptureParticlesDelay()
    {
        CaptureParticles.Play();
    }
}
